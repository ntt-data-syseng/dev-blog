import React, { useState } from "react";
import tw, { styled } from "twin.macro";

const SectionTitle = styled.h1`
  ${tw`font-bold text-lg cursor-pointer text-center`}
  ${(props) => (props.underline ? tw`underline` : ``)}
`;

const Index = ({ data, defaultOption }) => {
  const [selected, setSelected] = useState(data[defaultOption]);

  return (
    <div className="mx-auto">
      <div className="flex flex-row space-x-12 justify-center">
        {Object.keys(data).map((option) => (
          <SectionTitle
            underline={selected === data[option]}
            onClick={() => setSelected(data[option])}
          >
            {option}
          </SectionTitle>
        ))}
      </div>

        <div className="space-y-4 my-6">
            <h1 className="font-bold">{selected.key}</h1>
            {selected.coverageImage}
            {selected.explanation.map((explanation) => {
                return (
                    <p>
                        {explanation}
                    </p>
                )
            })} 
            <img src={selected.exampletestImage} />
        </div>
    </div>
  );
};

export default Index;
