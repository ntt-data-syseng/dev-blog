import React from "react";
import Header from "../header";

const PageTitle = ({ title, subtitles }) => (
  <section className="border-b py-12 text-black">
    <Header title={title} subtitles={subtitles} />
  </section>
);

export default PageTitle;
