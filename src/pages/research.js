import React from "react";
import Section from "../components/Section";
import PageTitle from "../components/PageTitle";
import GraphDatabase from "../images/research/graph-database.png";
import ExistingPlatform from "../images/research//existing-platform.png";
import NextLogo from "../images/research/nextjs-logo.png";
import NodeJsLogo from "../images/research/node-js-logo.png";
import FastAPI from "../images/research/fast-api.png";
import FrontendTrends from "../images/research/frontendTrends.png";
import LibraryResearch from "../images/research/library-performance.png";
import Sketch from "../images/research/sketch.jpg";
import DockerLogo from "../images/research/docker-logo.png";
import Picture1 from "../images/research/Picture1.png";
import AntV from "../images/research/antv.png";
import Picture2 from "../images/research/Picture2.png";
import Pdf1 from "../images/research/pdf1.png";
import Pdf2 from "../images/research/pdf2.png";
import Picture3 from "../images/research/Picture3.png";

const Research = () => {
  return (
    <>
      <PageTitle title="Research" />
      <Section title="Project Research">
        <p>
          Genome web applications facilitate a graphical interface for
          researchers to retrieve and analyse genomic data. Currently, there
          exist two genome browsers. One being a species-specific genome browser
          that focuses solely on one model organism [1]. The other form of
          application covers multiple species, integrating sequence and
          annotations for multiple organisms further promoting cross-species
          analysis.
        </p>
        <br />
        <p>
          The form of application we intend to develop follows the latter,
          allowing researchers to compare and analyse the differences and
          similarities of different gene clusters. From assessing different
          existing applications, we identified how most platforms were fairly
          outdated with complex usability flow and a poor user interface as
          shown in the figure below. This emphasised the importance of ensuring
          simplicity, both in aesthetics and usability, within our application
          allowing for an easier research process.
        </p>
        <img src={ExistingPlatform} style={{ marginTop: "30px" }} />
      </Section>

      <Section title="Neo4J Overview" shade>
        <div className="content">
          <p>Neo4j is a graph database management system developed by Neo4j.</p>
          <br />
          <p>
            The nature of our project required a graph database to present data
            in a visual, graphical and interactive format. It is highly
            unrealistic to model an ERD diagram with 100 of relationships, graph
            database offer a simpler alternative to connect data points
            together[2]. Further, it is also flexible if a relationship needs to
            change you do not need to perform database table migrations as you
            would on a normal relational database. This would also help
            complement the relationships that would need to stored between the
            different taxonomies and their KO numbers. Neo4j satisfied these
            requirements and so was an ideal solution for our database problem.
            It has a large support community and has been used by Netflix,
            AirBnB and other household names to perform data management[3]. We
            decided to first begin researching the different functionalities and
            capabilities of the database platform. This involved truly
            understanding what a graph database entailed.
          </p>
          <br />
          <p>
            Using the documentation and different video courses provided by the
            website[4], we were able to further comprehend how Neo4j would be
            incorporated into the application using cypher queries along with
            CSV formatted data. This gave us a better understanding of how to
            manipulate the data required before the build of the application. To
            further enhance our ability and understanding of Neo4j we also
            played around with the Neo4j Sandbox, using different cypher queries
            to act upon sample graph databases. For more advanced cypher queries
            that we will utilise like set theory(left hand set, right hand set,
            subset) Neo4j also has the APOC library which is a library of
            functions that provide more performant queries and extra features
            [5].
          </p>
        </div>
        <img
          src={GraphDatabase}
          width="500"
          height="500"
          alt="laptop with NTT Analysis software shown on screen"
          style={{ margin: "auto", marginTop: "20px" }}
        />
      </Section>

      <Section title="Tech Stack">
        <p>
          We decided that NextJS was a suitable front-end framework for our
          application as it provided a simple platform which we had previous
          experience in developing applications with. We will also use NodeJS
          for parts of the backend, Node is a very commonly used backend engine
          and allows for a synonymous programming language across our
          application. This will be used along with Fast API for handling
          requests in order to have full control over how users access data. As
          mentioned above we intend on using Neo4j as our graph database to
          store all the required data that we will process. We intend to also
          use a traditional relational database PostgreSQL to facilitate the
          storage of user credentials when authenticating our users. This will
          also allow users to save an analysis which they can revisit at a later
          date. The entire project will also be packaged into Docker for an
          improved development experience and easier project transportation [6].
          Our research in the sections below will highlight why we have chosen
          this tech stack for this application.
        </p>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            marginTop: "50px",
          }}
        >
          <img
            style={{
              width: "100px",
              height: "100px",
              marginRight: "45px",
              marginLeft: "45px",
            }}
            src={NextLogo}
          />
          <img
            style={{
              width: "100px",
              height: "100px",
              marginRight: "45px",
              marginLeft: "45px",
            }}
            src={NodeJsLogo}
          />
          <img
            style={{
              width: "150px",
              height: "60px",
              marginRight: "45px",
              marginLeft: "45px",
            }}
            src={FastAPI}
          />
          <img
            style={{
              width: "100px",
              height: "100px",
              marginRight: "45px",
              marginLeft: "45px",
            }}
            src={DockerLogo}
          />
        </div>
      </Section>

      <Section title="Extracting the data" shade>
        <p>
          Extensive research was required to first understand where to extract
          the data from and what exactly what was needed. The “kegg.jp”
          database[7] was found where all the data regarding every time of
          organism from viruses to humans is stored. The team then found within
          the database the 8 different strains required for us to complete this
          project. See the image below:
        </p>
        <br />
        <img src={Picture1} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          Entering each strain brings up the information about that particular
          version of the virus as seen below:
        </p>
        <br />
        <img src={Picture2} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          To find the KO numbers recorded (gene clusters) for that particular
          organism the “Taxonomy” button found under sequence must be clicked.
          Finally the KO numbers will be displayed which have their own data
          stored such as the name, number and so on. That data required to be
          extracted was the 8 taxonomies and their information as well as all
          the gene clusters for each taxonomy and their information.
        </p>
      </Section>

      <Section title="Frontend Research">
        This application will have a lot of interactivity, as such we need a
        frontend library. There are three main competitors which are React,
        Angular and Vue. The figure below [8] shows the trend of downloaded
        packages for these frontend libraries. As with a major dependency of the
        application we have to inspect them based on these three criteries which
        are popularity, support and community resources and performance.
        <img src={FrontendTrends} />
        <br />
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Angular</h1>
        Angular is frontend <b>Framework</b> created by Google. This means that
        it comes with all the functionality such as the ability to make requests
        whereas other libraries like React and Vue has to find their own
        independent third party libraries. It is often utilised as part of the
        popular MEAN stack. Some if its highlights are that it has{" "}
        <b>Two-way data binding</b>[9], this means that there is a single source
        of truth and that the webpage is always synchronised to. (Note: React
        can also write code to be able to do this through the useState API)
        Another advantage is that it also has dependency injection which means
        that it is easier for testing as well as adding functionality to
        existing components.
        <br />
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Vue</h1>
        Vue could be thought of as a lighter-weight version of Angular. It is
        also the newest of the three framework/library of our choices today. It
        also has quite an easy learning curve when compared to Angular whose
        force usage of typescript is the norm[10]. However being a new framework
        there is a small community that has built resources and support around
        it. It also does not have an advantage of being supported by a big
        organisation like Google(Angular) or Facebook(React).
        <br />
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>React</h1>
        React frontend library is the most popular library for bulding user
        interfaces on the web. Being a library it has no tools built in like the
        other tools that come with functionality such as an API to make a
        request from [11]. With React you would need to install Axios or other
        http library to make the requests. This allows it to be much more
        adaptable than the other two and due to its large ecosystem and
        popularity there are many people who have built specific tools that has
        great support for it with React. Furthermore, there are more tutorials
        and resources that are based or catered to people who uses React so if
        there are any issues it is more than likely that other people have the
        same issues and have come up with a fix.
        <br />
        <br />
        <h1
          style={{ fontSize: "17px", fontWeight: "800", textAlign: "center" }}
        >
          Duration in milliseconds ± standard deviation (Slowdown = Duration /
          Fastest) [12]
        </h1>
        <div style={{ margin: "0 auto" }}>
          <img
            src={LibraryResearch}
            style={{ textAlign: "center", margin: "0 auto" }}
          />
        </div>
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Conclusion</h1>
        From the short comparison above we have concluded that using React is
        the best option in terms of support and third party libraries. We also
        used the public benchmark that other developers has created to compare
        the performance between the three libraries. From above it overall it
        shows that React is between Angular and Vue in terms of performance.
        This means that it is at least on par with the rest of the frameworks.
        The popularity behind it has also spawned many projects that are catered
        to specific problems. For the project we will use NextJS, which is based
        on React, that has built-in SSR support[13]. NextJS being based on React
        also has given it access to all the React libraries and support. One of
        the core essential library we will use to manage the state of our
        application is Redux[14]. To compare this SSR feature with the other two
        frameworks Angular does not have support for it or even an off-shoot
        project something like NextJS for it. VueJS has SSR built in but suffers
        from having to synchronise the state of the application yourself,
        something that NextJS can do automatically by just installing a library.
      </Section>

      <Section title="Frontend Graph Library Research" shade>
        We plan to represent an analysis of the connection between the viruses
        as a graph database. Where the common nodes in the center will be the
        common features of the viruses. These common nodes will then link the
        two viruses together and also displaying unique nodes of each virus on
        their respective sides. A sketch of our visualisation can be seen below.
        A comparison between the graph generation library has been compared [15]
        and for our use case G6 has the best fit for our application. G6
        provides a balance of extendability, customisation with pre-made
        features. A feature that we need with G6 that is not available on many
        other libraries were the ability to customise the logic for positioning
        on the nodes on the canvas, which is essential to not overlap the nodes
        when the user drags them (disable user from being able to place on node
        on top of another to block the view).
        <img src={Sketch} width="100%" />
      </Section>

      <Section title="Authentication System Research">
        <p>
          To enhance the user experience and provide extra features for
          recurring users the team decided to set up a simple authentication
          system with Node, Express and Postgress.
          <br />
          <br />
          Below is a diagram of how a session cookie is issued (/login) and then
          used to make an API call to another service (/api) in a nutshell:
        </p>
        <br />
        <img src={Picture3} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          The team decided to use session cookie for 2 reasons.
          <br />
          <br />
          1. It allows session based authentication, meaning if the user is not
          active for a short period of time they are automatically logged out
          <br />
          <br />
          2. Can perform centralised authentication through the API Gateway
          Microservice pattern [16]
          <br />
          <br />
        </p>
        <br />
        <p>
          In a microservices setup, each microservice can independently verify
          that a user is authenticated. The microservice can access these
          information through the req object and need not have access to a
          centralized token database, hence the team felt that this system would
          be more efficient and reliable.
        </p>
        <br />
        <p>
          The team also decided to implement Redis as its data resides in
          memory, which enables low latency and high throughput data access.
          Unlike traditional databases, In-memory data stores does not require a
          trip to disk, reducing engine latency to microseconds.[17] Because of
          this, in-memory data stores can support an order of magnitude more
          operations and faster response times. The result is blazing-fast
          performance with average read and write operations taking less than a
          millisecond and support for millions of operations per second.
        </p>
      </Section>

      <Section title="PDF research" shade>
        <p>
          Part of the requirements for the project was the PDF generation of the
          graph, extensive research was done into how to implement this. There
          are two ways in which a pdf could be generated, in the frontend or the
          backend. Below are the pros and cons from the research.
        </p>
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Backend</h1>
        <h1 style={{ fontSize: "15px", fontWeight: "800" }}>Pros:</h1>
        <p>
          1. Formatting is guaranteed as different browsers will have slightly
          different styling 2. Can handle larger data sets because generally,
          servers have greater resources available than browsers do
        </p>
        <h1 style={{ fontSize: "15px", fontWeight: "800" }}>Cons:</h1>
        <p>
          1. Without proper taskqueue, becomes a very easy target to overload
          the server(DDOS). Spamming the generation of 100 pdf for example.
        </p>
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Frontend</h1>
        <h1 style={{ fontSize: "15px", fontWeight: "800" }}>Pros:</h1>
        <p>
          1. Simple solution, don’t need to hand off data to the front user. All
          work is done on the frontend 2. The PDF generation process does not
          require an internet connection
        </p>
        <h1 style={{ fontSize: "15px", fontWeight: "800" }}>Cons:</h1>
        <p>
          1. Unable to handle larger information like images (when file exceeds
          8mb) 2. Limited by the device of the client. If the user’s device is
          old then can be a problem if they want to generate a report.
        </p>
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>Conclusion</h1>
        <p>
          The initial conclusion was to implement the PDF generation on the
          frontend. This is due to the fact that from an architectural
          perspective it is simpler to implement[18]. However, we moved this to
          the backend. The reason for this is to cache each pdf for different
          taxonomy combinations and store them somewhere. Hence, when the user
          requests for a pdf the application will just extract the pdf from
          storage a nd hand it to the user. This allows for the pdf to be
          returned to the client in one request/response cycle without needing
          to hand off the pdf generation to a background task queue.
          <br />
        </p>
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>
          Comparison of Libraries
        </h1>
        <p>
          When comparing libraries it can be noticed that the main difference is
          how the api is exposed and the style[19]. From most libraries the
          underlying logic is based on pdfkit, with the exception of jsPDF.
        </p>
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>PDFLIB</h1>
        <p>
          This library is eliminated as it has no native way to allow us to
          create a table and has no supportive library that does so. Hence
          drawing lines to make the table ourselves is required which would be
          very time consuming.
        </p>
        <br />
        <img src={Pdf1} style={{ marginTop: "30px" }} />
        <br />
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>PDFMake</h1>
        <p>
          PDFmake was also eliminated. After having a glance at the github repo
          of the library it shows that the current master branch is unstable.
          And that the next upcoming version has many features that are yet to
          be implemented.
          <br />
          <br />
          This left the team with either PDFkit or jsPDF. Both of these
          libraries have almost the same features. Which leads into the next
          consideration that we have, the file size.
        </p>
        <br />
        <br />
        <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
          Comparison between PDFKit and jsPDF
        </h1>
        <img src={Pdf2} style={{ marginTop: "30px" }} />
        <br />
        <br />
        <p>
          From the comments above the file size generated for identical content
          is larger on the pdfkit. Hence since both of these libraries provide
          almost one-to-one feature jsPDF[20] was chosen to be implemented.
        </p>
      </Section>

      <Section title="Technical Decisions" shade>
        <div
          className="table"
          style={{
            width: "800px",
            height: "400px",
            display: "flex",
            border: "1px solid #DFDFDF",
            borderRadius: "7px",
          }}
        >
          <div
            className="leftSection"
            style={{
              width: "50%",
              marginRight: "auto",
              padding: "20px 30px 20px 30px",
            }}
          >
            <p
              style={{
                paddingBottom: "12px",
                borderBottom: "1px solid #DFDFDF",
                fontSize: "12px",
              }}
            >
              TECH
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              Front-end Framework
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              Graph Visualiser
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              Back-end Framework
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              Graph Database
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              External API
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              REST API
            </p>
          </div>
          <div
            className="rightSection"
            style={{
              width: "50%",
              marginLeft: "auto",
              padding: "20px 30px 20px 30px",
            }}
          >
            <p
              style={{
                paddingBottom: "12px",
                borderBottom: "1px solid #DFDFDF",
                fontSize: "12px",
              }}
            >
              DECISION
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              NextJS
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              G6
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              Node js and Fast API
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              Neo4j
            </p>
            <p
              style={{
                paddingBottom: "12px",
                paddingTop: "12px",
                borderBottom: "1px solid #DFDFDF",
              }}
            >
              KEGG
            </p>
            <p style={{ paddingBottom: "12px", paddingTop: "12px" }}>Docker</p>
          </div>
        </div>
      </Section>

      <Section title="References">
        <p>
          [1] Wang, J., Kong, L., Gao, G. and Luo, J., 2022. A brief
          introduction to web-based genome browsers. [online] academic.oup.com.
          Available at:
          &lt;https://academic.oup.com/bib/article/14/2/131/208726&gt; [Accessed
          3 April 2022].
        </p>
        <br />
        <p>
          [2] Neo4j Graph Data Platform. 2022. Why Graph Databases?. [online]
          Available at:&lt;https://neo4j.com/why-graph-databases/&gt; [Accessed
          2 April 2022].
        </p>
        <br />
        <p>
          [3] Neo4j Graph Data Platform. 2022. Why Neo4j? Top Ten Reasons.
          [online] Available at: &lt;https://neo4j.com/top-ten-reasons/&gt;
          [Accessed 3 April 2022].
        </p>
        <br />
        <p>
          [4] Neo4j Graph Data Platform. 2022. 3 - Building Great, Simple APIs
          for your Neo4j Projects. [online] Available at:
          &lt;https://neo4j.com/videos/3-building-great-simple-apis-for-your-neo4j-projects/&gt;
          [Accessed 4 April 2022].
        </p>
        <br />
        <p>
          [5] Neo4j Graph Data Platform. 2022. Neo4j APOC Library - Developer
          Guides. [online] Available at:
          &lt;https://neo4j.com/developer/neo4j-apoc/&gt; [Accessed 3 April
          2022].
        </p>
        <br />
        <p>
          [6] Education, I., 2022. What is Docker?. [online] Ibm.com. Available
          at: &lt;https://www.ibm.com/cloud/learn/docker&gt; [Accessed 3 April
          2022].
        </p>
        <br />
        <p>
          [7] Genome.jp. 2022. KEGG GENOME Database. [online] Available at:
          &lt;https://www.genome.jp/kegg/genome/&gt; [Accessed 3 April 2022].
        </p>
        <br />
        <p>
          [8] Npmtrends.com. 2022. angular vs react vs vue | npm trends.
          [online] Available at:
          &lt;https://www.npmtrends.com/angular-vs-react-vs-vue&gt; [Accessed 4
          April 2022].
        </p>
        <br />
        <p>
          [9] Angular.io. 2022. Angular. [online] Available at:
          &lt;https://angular.io/guide/two-way-binding&gt; [Accessed 4 April
          2022].
        </p>
        <br />
        <p>
          [10] AltexSoft. 2022. The Good and the Bad of Vue.js Framework
          Programming. [online] Available at:
          &lt;https://www.altexsoft.com/blog/engineering/pros-and-cons-of-vue-js/&gt;
          [Accessed 4 April 2022].
        </p>

        <br />
        <p>
          [11] freeCodeCamp.org. 2022. Is React a Library or a Framework? Here's
          Why it Matters. [online] Available at:
          &lt;https://www.freecodecamp.org/news/is-react-a-library-or-a-framework/&gt;
          [Accessed 4 April 2022].
        </p>
        <br />
        <p>
          [12] Stefankrause.net. 2022. Interactive Results. [online] Available
          at:
          &lt;https://stefankrause.net/js-frameworks-benchmark8/table.html&gt;
          [Accessed 4 April 2022].
        </p>

        <br />
        <p>
          [13] The Software House. 2022. What is Next.js used for? Introduction
          to Next.js for CTOs. [online] Available at:
          &lt;https://tsh.io/blog/what-is-next-js-used-for/> [Accessed 4 April
          2022].
        </p>
        <br />
        <p>
          [14] React-redux.js.org. 2022. Why Use React Redux? | React Redux.
          [online] Available at:
          &lt;https://react-redux.js.org/introduction/why-use-react-redux&gt;
          [Accessed 4 April 2022].
        </p>
        <br />
        <p>
          [15] Cylynx.io. 2022. A Comparison of Javascript Graph / Network
          Visualisation Libraries. [online] Available at:
          &lt;https://www.cylynx.io/blog/a-comparison-of-javascript-graph-network-visualisation-libraries/&gt;
          [Accessed 4 April 2022].
        </p>
        <br />
        <p>
          [16] microservices.io. 2022. Microservices Pattern: API gateway
          pattern. [online] Available at:
          &lt;https://microservices.io/patterns/apigateway.html&gt; [Accessed 4
          April 2022].
        </p>
        <br />
        <p>
          [17] Amazon Web Services, Inc. 2022. Redis: in-memory data store. How
          it works and why you should use it. [online] Available at:
          &lt;https://aws.amazon.com/redis/&gt; [Accessed 3 April 2022].
        </p>
        <br />
        <p>
          [18] Medium. 2022. Most Efficient Ways For Building PDFs Files With
          JavaScript. [online] Available at:
          &lt;https://javascript.plainenglish.io/most-efficient-ways-for-building-pdfs-files-with-backend-and-frontend-javascript-environment-68056f73257&gt;
          [Accessed 4 April 2022].
        </p>
        <br />
        <p>
          [19] DEV Community. 2022. A full comparison of 6 JS libraries for
          generating PDFs. [online] Available at:
          &lt;https://dev.to/handdot/generate-a-pdf-in-js-summary-and-comparison-of-libraries-3k0p&gt;
          [Accessed 4 April 2022].
        </p>
        <br />
        <p>
          [20] Parallax. 2022. jsPDF - HTML5 PDF Generator | Parallax. [online]
          Available at: &lt;https://parall.ax/products/jspdf&gt; [Accessed 4
          April 2022].
        </p>
      </Section>
    </>
  );
};

export default Research;
