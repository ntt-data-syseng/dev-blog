import React from "react";
import PageTitle from "../components/PageTitle";
import Section from "../components/Section";

const Blog = () => {
  return (
    <>
      <PageTitle title="Blog" />
      <Section title="Week 1/2">
        <p>
          The blog posting for weeks 1/2 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-1-and-2-f875b7473817"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF format
          <a
            href="./Week_12.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 3/4">
        <p>
          The blog posting for weeks 2/3 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-3-and-4-926233205037"
            target="_blank"
            and
            is
            also
            available
            in
            PDF
            format
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF format
          <a
            href="./Week_34.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 5/6">
        <p>
          The blog posting for weeks 5/6 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-5-and-6-98abd1e49eaf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF format
          <a
            href="./Week_56.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 7/8">
        <p>
          The blog posting for weeks 7/8 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-7-and-8-1e414181e69a"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF
          <a
            href="./Week_78.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 9/10">
        <p>
          The blog posting for weeks 9/10 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-9-and-10-f220057ee038"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF format
          <a
            href="./Week_910.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 11/12">
        <p>
          The blog posting for weeks 11/12 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-11-and-12-5aa0f8cf89c4"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF format
          <a
            href="./Week_1112.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 13/14">
        <p>
          The blog posting for weeks 13/14 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-13-and-14-3773c6dc05a5"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF format
          <a
            href="./Week_1314.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 15/16">
        <p>
          The blog posting for weeks 15/16 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-15-and-16-7548b205c7de"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also avaible in PDF format
          <a
            href="./Week_1516.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
      <Section title="Week 17/18">
        <p>
          The blog posting for weeks 17/18 is archived on Medium
          <a
            href="https://medium.com/@mynameisgan10/week-17-and-18-bf8e4a86e94e"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
            rel="noreferrer"
          >
            here
          </a>{" "}
          and is also available in PDF format
          <a
            href="./Week_1718.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>
    </>
  );
};

export default Blog;
