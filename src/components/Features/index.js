import React from "react";
import tw, { styled } from "twin.macro";

import {
  AimOutlined,
  CopyFilled,
  EyeFilled,
  BookFilled,
  RightCircleFilled,
  SafetyCertificateFilled,
} from "@ant-design/icons";
import Section from "../Section";

const FeaturesContainer = styled.div`
  ${tw`grid grid-rows-6 md:grid-cols-3 md:grid-rows-1 justify-center text-xl text-blue-900 font-bold opacity-100`}
`;

const index = () => {
  return (
    <Section title="Features">
      <FeaturesContainer>
        <span className="p-6 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-6 justify-center items-center">
            <EyeFilled style={{ color: "#1e40af", fontSize: 40 }} />
            <div>Visual</div>
          </div>
        </span>

        <span className="p-6 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-6 justify-center items-center">
            <AimOutlined style={{ color: "#1e40af", fontSize: 40 }} />
            <div>Drill Down</div>
          </div>
        </span>

        <span className="p-6 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-6 justify-center items-center">
            <CopyFilled style={{ color: "#1e40af", fontSize: 40 }} />
            <div>Generate Report</div>
          </div>
        </span>

        <span className="p-6 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-6 justify-center items-center">
            <RightCircleFilled style={{ color: "#1e40af", fontSize: 40 }} />
            <div>Collaborate</div>
          </div>
        </span>

        <span className="p-6 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-6 justify-center items-center">
            <SafetyCertificateFilled
              style={{ color: "#1e40af", fontSize: 40 }}
            />
            <div>Authentication</div>
          </div>
        </span>

        <span className="p-6 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-6 justify-center items-center">
            <BookFilled style={{ color: "#1e40af", fontSize: 40 }} />
            <div>Bookmarking</div>
          </div>
        </span>
      </FeaturesContainer>
    </Section>
  );
};

export default index;
