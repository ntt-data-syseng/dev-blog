import * as React from "react";
import Header from "../components/header";
import UserCard from "../components/UserCard";
import Features from "../components/Features";
import LandingImage from "../images/index/landing.png";
import Section from "../components/Section";
import { Problems } from "../data/landing";
import CardWithIcon from "../components/CardWithIcon";
import tw, { styled } from "twin.macro";
import AmmaarImage from "../images/index/ammaar-profile.jpeg";
import ZainImage from "../images/index/zain-profile.jpeg";
import EricImage from "../images/index/eric-profile.jpeg";
import GanttChart from "../images/index/gantt-chart.png";
import YouTubeEmbed from "../components/YouTubeEmbed";

const MockUpContainer = styled.div`
  ${tw`flex flex-col md:flex-row md:items-center md:justify-center mx-12 md:px-24`}
`;

const IndexPage = () => {
  return (
    <>
      <section className="border-b pt-12 md:pb-12 text-black">
        <MockUpContainer>
          <div className="w-full md:w-7/12">
            <Header
              title="Application to Analyse Gene Clusters"
              subtitles={[
                "Team 27 - UCL Computer Science",
                "Zain Saleem - Ammaar Bin-Maajid - Passawis Chaiyapattanaporn",
              ]}
            />
          </div>
          <div className="w-full md:w-5/12">
            <img
              src={LandingImage}
              width="800"
              height="600"
              alt="laptop with NTT Analysis software shown on screen"
            />
          </div>
        </MockUpContainer>
      </section>
      <Section title="Abstract" shade>
        <p>
          NTT DATA is a Japanese multinational information technology service
          and consulting company. They help clients transform through
          consulting, industry solutions, business process services, IT
          modernization and managed services. NTT DATA are looking for a new
          software that enables researchers to analyse and compare different
          strains of the coronavirus. Currently, there is data available in the
          public domain from the KEGG orthology database. However, it becomes
          very difficult for researchers to analyse the gene commonalities
          between different strains of the virus due to the enormity of the
          dataset and inconsistencies in the API.
        </p>
        <p className="mt-12">
          We intend to solve this issue by developing a web application purposed
          for providing graphical information on the similarities and
          differences between different gene clusters. Considering the
          complexity of traditional relational databases involving join
          commands, we plan on visually storing the data in a graph database
          using neo4J. This will then provide the graphical representation for
          which we can display the necessary data.
        </p>
        <p className="mt-12">
          This will ultimately transform the large inconsistent data to a
          significantly more digestible format aiding the analysis procedure for
          researchers. Researchers will then be encouraged to record their
          discoveries through a built-in report generation which can then be
          shared amongst other researchers for further collaboration.
        </p>
      </Section>
      <Section title="The Problem">
        <h3 className="text-xl text-black text-center">
          Visual analysis of common gene clusters in disease causing viruses in
          the Coronaviridae category
        </h3>
        <div className="w-full mb-4">
          <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
        </div>

        <div className="flex flex-col items-center space-y-8 lg:flex-row lg:flex-wrap lg:justify-start lg:space-y-0 lg:space-x-6">
          {Problems.map((data) => (
            <CardWithIcon title={data.title} content={data.content} />
          ))}
        </div>
      </Section>

      <Section title="Achievements" shade>
        <p>
          The team succeeded in implementing all the requirements set out by the
          client. The common and unique gene clusters between two chosen
          coronavirus strains are visually represented in an aesthetically
          pleasing bloom graph. The users are also able filter out common and
          unique nodes to be more precise in what is being viewed. In addition
          to this the user can generate a PDF version of the analysis to
          continue work offline. The user can also find out further information
          about a specific node by selecting the chosen node, resulting in the
          appetence of a sidebar presenting the further information required. In
          addition, the user can easily share the graph with the collaboration
          feature, and finally the user can create an account via the
          authentication system. This allows the user to then bookmark specific
          graphs to return to them easier at a later date.
        </p>
      </Section>

      <Features />

      <Section title="Project Video" shade>
        <p
          style={{
            width: "800px",
          }}
        ></p>
        <YouTubeEmbed embedId="tPRV3crDM9w" />
      </Section>

      <Section title="Meet our Team">
        <div className="grid grid-rows-3 md:grid-cols-3 md:grid-rows-1 gap-4">
          <UserCard
            name={"Ammaar Bin-Maajid"}
            image={AmmaarImage}
            email="zcabbin@ucl.ac.uk"
            github="https://github.com/ambm2000"
            linkedIn="https://www.linkedin.com/in/ammaar-nawaz-581799203/"
            content="Ammaar is a second year computer science student at UCL who has a passion for cyber security, data analysis and data science. "
            roles="Blog devloper, researcher, website designer/developer, video editor, client liaison"
          />

          <UserCard
            name={"Passawis Chaiyapattanaporn"}
            image={EricImage}
            email="zcabpc0@ucl.ac.uk"
            github="https://github.com/ericma1999"
            linkedIn="https://www.linkedin.com/in/passawis/"
            content="Passawis is pursuing a MEng in Computer Science at UCL and has great passion for data science and finance. He is eager to learn new technologies and tools and utilising to tackle problems and projects."
            roles="Back-end developer, Researcher, Website designer/developer, Client liaison, Tester"
          />

          <UserCard
            name={"Zain Saleem"}
            image={ZainImage}
            email="zain.saleem@ucl.ac.uk"
            github="https://github.com/zains719"
            linkedIn="https://www.linkedin.com/in/zains719/"
            content="Zain is currently pursuing a BSc in Computer Science at University College London (2nd year) and has a great passion for application development and data science. He is always always keen to learn new technologies and work on exciting, challenging projects."
            roles="Front-end developer, Researcher, Website designer/developer, Client Liaison, Tester"
          />
        </div>
      </Section>

      <Section title="Project Management - Gantt Chart" shade>
        <img src={GanttChart} />
      </Section>
    </>
  );
};

export default IndexPage;
