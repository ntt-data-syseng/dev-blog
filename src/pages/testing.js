import React from "react";

import PageTitle from "../components/PageTitle";
import Section from "../components/Section";
import UserAcceptance from "../images/testing/userAcceptance.png";
import FrontendTest from "../images/testing/frontendTest.png";
import passedTests from "../images/testing/passedTests.png";
import testFigures from "../images/testing/testFigures.png";
import coverage from "../images/testing/coverage.png";
import coverageCtd from "../images/testing/coverageCtd.png";
import NTTAUTH from "../images/testing/ntt-auth.png";
import NTTPDF from "../images/testing/ntt-pdf.png";
import GraphDB from "../images/testing/graph-db.png";
import GraphSBSetup from "../images/testing/graph-db-setup.png";
import GraphDBExample from "../images/testing/graph-db-example.png";
import PDFSuccessCase from "../images/testing/ntt-pdf-success-case.png";
import PDFFailCase from "../images/testing/ntt-pdf-fail-case.png";
import BuildExample from "../images/testing/buildExample.jpeg";
import BuildOverview from "../images/testing/buildOverview.jpeg";
import RepoHome from "../images/testing/repoHome.jpeg";
import CIBuilding from "../images/testing/activeCI.png";

const Testing = () => {
  return (
    <>
      <PageTitle title="Testing" />
      <Section title="Testing Strategy">
        <p>
          The Gene Cluster Analysis (GCA) platform is a client side application.
          Therefore, it is essential to execute thorough testing to ensure that
          the usability, stability and functionalities of the platform satisfy
          the initial requirements from our client, defined in the MOSCOW list.
          Through implementing different testing variations, we were able to
          achieve a code coverage of over 97% as as shown below, allowing us to
          deliver our product to our client with great confidence.
        </p>
        <br />

        <img src={coverage} />
        <img src={coverageCtd} />

        <br />
        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>Scope</h2>
        <p>
          There are different functionalities that exist within the platform and
          are developed using a combination of various packages and libraries.
          Each functionality was tested and stretched to its maximum to ensure
          they are working efficiently and can be utilised by users without any
          difficulties or constraints.
        </p>
        <br />
        <p>
          The GCA should be tested with user-behaviour taken into consideration.
          Since the platform is primarily designed for researchers, the testing
          should mimic their behaviour by considering the different patterns and
          preferences for these types of users. For example, this involves
          assessing the complexity in which the platform can be utilised and how
          efficiently an analysis can be performed.
        </p>
        <br />
        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>
          Testing Methodology
        </h2>
        <p>
          We decided to implement a variation of testing methods including both
          automated unit and integration testing along with user acceptance from
          our target customer segment. Unit testing allowed us to first design
          and outline the specific requirements for each component in our
          application. These could then be individually tested in an isolated
          case. This eased the process of debugging and identifying error points
          in our code. Once we were confident with the correctness of these
          individual components, we were able to integrate the entire platform
          together and carry out integration testing. This ultimately allowed us
          to assess the overall performance and resilience of the platform.
        </p>
      </Section>
      <Section title="Frontend Unit Testing" shade>
        <p>
          As mentioned above, we first began implementing front end unit tests
          in order to define the specific functionality of individual components
          and ensure they are successfully operating. This also included testing
          the Redux components created such as reducers, selectors and actions.
          The unit tests were written using a combination of React Testing
          Library - the default React testing tool that allows you to actually
          test the DOM tree rendered by React on the browser, and Jest - a
          javascript testing framework allowing us to essentially run different
          tasks. We were able to successfully construct and pass 86 tests
          ensuring a great deal of robustness in our application.
        </p>
        <div
          style={{
            display: "flex",
            marginTop: "25px",
          }}
        >
          <img
            src={passedTests}
            style={{ marginRight: "20px", width: "500px" }}
          />
          <img
            src={testFigures}
            style={{ marginLeft: "20px", width: "300px", height: "200px" }}
          />
        </div>
        <br />

        <p>
          The React Testing Library enabled us to easily construct our different
          tests with the easy mocking of functions in jest. It also provided
          clear code coverage reports allowing us to maximise the reach of our
          tests. The mocking functionality allowed us to mock different
          functions as well as create mock routers and dispatchers where needed.
        </p>

        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>
          Component Testing
        </h2>
        <p>
          Most component tests consisted of rendering the specific component and
          then using a data-test-id to obtain a reference to the mounted
          component. The necessary actions could then be called upon it, such as
          an onclick for a button. We could then use the expect function to
          assert the result of these actions to the desired outcome. This is
          demonstrated in a screenshot of the toolbar test below. The toolbar
          component was required to be wrapped in a provider in order to mock
          the dispatcher. We could then assert the state of different components
          of the toolbar depending on whether the toolbar prompt button was
          clicked or not.
        </p>
        <img src={FrontendTest} style={{ marginTop: "15px" }} />

        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>Redux Testing</h2>
        <p>
          The redux tests differ slightly as they do not exactly refer to
          functionality of a specific component but have more complex use cases.
          The selector testing required us to use the resultFunc property on
          which we call the function with its required parameters whilst
          reducers allowed us to manage and test the changes in state upon the
          different actions defined. The individual actions were tested
          separately where each request was called and asserted against its
          expected response. The actions thunk required some more pre-test
          manipulation. We were first to create a mock store using middleware
          and thunk. We also created a mock adapter in order to mock the axios
          requests that were being made. Both these mocks were restored before
          and after each test.
        </p>

        <br />

        <p>
          We also individually tested the separate backend components of our
          application. This inclined testing the individual routes that would be
          called for their respective functionalities. We would then assert the
          outcome of these requests and match them to what was desired.
        </p>
      </Section>
      <Section title="Backend Services Unit Testing">
        <p>
          To ensure that each of the services are able to handle their tasks
          correctly. Unit tests for each of the endpoints were conducted. These
          contain different cases that a request to the backend might occur.
          Some test cases that we have created for different situation were when
          the user is not authenticated, or the format of the request is wrong
          for example. For each of the backend services, not including the
          NTT-GATEWAY, tests were written for each endpoint to test that they
          perform correctly. An overview of the tests that was wrote can be seen
          in the figures below.
        </p>
        <br />
        <br />
        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>NTT-AUTH Tests</h2>
        <br />
        <br />
        <img src={NTTAUTH} />
        <br />
        <br />
        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>NTT-PDF Tests</h2>
        <br />
        <br />
        <img src={NTTPDF} />
        <br />
        <br />
        For testing the PDF generation service, we have created two test cases.
        This is a simple service, however the format of the content being posted
        to the endpoint must be correct for the PDF generated to have the
        correct layout.
        <br />
        <br />
        <img src={PDFSuccessCase} />
        <br />
        <br />
        In the sample test above we tested that the format provided to generate
        the PDF was correct. If so the PDF report must be selected and we
        checked that the cache with the unique pdf id has been created for that
        analysis.
        <br />
        <br />
        <img src={PDFFailCase} />
        <br />
        <br />
        From the above figure we tested that the format is incorrect and that
        the resource should return the correct response format as shown.
        <br />
        <br />
        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>NTT-Graph Tests</h2>
        <br />
        <br />
        <img src={GraphSBSetup} />
        <br />
        <br />
        <p>
          To test the NTT-GRAPH(Graph-DB) we mock the database by using a Python
          fixture. The python fixture will create the database and drop the
          database before and after each tests. This ensure that there are no
          side effects from the previous tests affecting the current one. This
          can be seen in the figure above where create_all and drop_all with
          yield in the middle is used.
        </p>
        <br />
        <br />
        <img src={GraphDB} />
        <br />
        <br />
        <img src={GraphDBExample} />
        <br />
        <br />
        <p>
          To test our service, for each route we test for fail and success
          cases. In the sample in the figure above, it should fail since the
          mock database that we have populated for the test does not contain the
          taxonomy id being asked to analyse. The tests similar to the one in
          the above figure is then repeated for all the endpoints that is in the
          NTT-Graph(Graph-DB) service and contains a total of 18 tests.
        </p>
      </Section>
      <Section title="User Acceptance Testing">
        <p>
          To further understand how effectively the GCA meets user’s needs and
          potential areas of improvement, we conducted 4 user acceptance testing
          interviews from varying backgrounds within our target customer
          segments. Provided below is the feedback related back from the users.
        </p>

        <br />
        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>Testers</h2>
        <p>
          Testers were strategically selected to possess characteristics
          representative to that of our target consumers (professors,
          researchers etc.).
        </p>
        <br />
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "400px",
            }}
          >
            <h2>Tester 1:</h2>
            <p>Bryan - Postgraduate student at UCL</p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "400px",
            }}
          >
            <h2>Tester 2:</h2>
            <p>Clara - Researcher at UCL</p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "400px",
            }}
          >
            <h2>Tester 3:</h2>
            <p>Francesca - Professor/researcher at UCL</p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "400px",
            }}
          >
            <h2>Tester 4:</h2>
            <p>Louis - Professor/researcher at UCL</p>
          </div>
        </div>
        <br />
        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>Test Cases</h2>
        <p>
          The test was separated into 4 separate test cases which involved users
          completing a specific task on the web application. We then recorded
          the simplicity and effectiveness in which these users were able to
          successfully complete these tasks through different metrics. This
          involved rating each requirement through a scale and leaving custom
          comments.
        </p>
        <br />
        <h2>Test Case 1:</h2>
        <p>
          Instruct users to conduct the analysis of two organisms and generate a
          bloom graph consisting of only the common gene clusters of the two
          organisms.
        </p>
        <br />
        <h2>Test Case 2:</h2>
        <p>
          From the point of a generated bloom graph - generate a pdf detailing
          the analysis completed.
        </p>
        <br />
        <h2>Test Case 3:</h2>
        <p>
          Direct users to the login page where they can sign up - generate a
          bloom graph and bookmark this into their dashboard. Reload the page
          and load the bookmarked analysis.
        </p>
        <br />
        <h2>Test Case 4:</h2>
        <p>
          Share a link to fellow researchers such that they can view an analysis
          of two particular organisms.
        </p>

        <br />
        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>Feedback</h2>
        <img src={UserAcceptance} />

        <br />
        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800" }}>
          Additional Comments
        </h2>
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "450px",
            }}
          >
            <h2 style={{ textDecoration: "underline" }}>Clean UI:</h2>
            <p>
              - Very clean and visually appealing user interface
              <br />
              - Clear calls of action
              <br />- Sidebar contains large blocks of info which is hard to
              digest
            </p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "450px",
            }}
          >
            <h2 style={{ textDecoration: "underline" }}>
              Sufficient PDF Summary:
            </h2>
            <p>
              - Amazing analysis digest that captures all necessary info and can
              be used for offline research activities
              <br />- Tables could be slightly better formatted with the
              miscellaneous data
            </p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "450px",
            }}
          >
            <h2 style={{ textDecoration: "underline" }}>
              Collaboration of Research:
            </h2>
            <p>
              - Useful link sharing feature to avoid having to share tedious
              specific details when collaborating
              <br />
              - PDF generation facilitates offline research collaboration in
              labs etc.
              <br />- Not many other possibilities for further collaboration -
              online research sessions
            </p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "450px",
            }}
          >
            <h2 style={{ textDecoration: "underline" }}>
              Visual Display/Interaction of Data:
            </h2>
            <p>
              - Incredible graphical visuals making the data a lot more
              digestible
              <br />- Cool graph interaction feature
            </p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "450px",
            }}
          >
            <h2 style={{ textDecoration: "underline" }}>
              Scope For Further Exploration:
            </h2>
            <p>
              - Provides detailed information and necessary links
              <br />- Data is not very digestible as it is just dumped onto the
              sidebar
            </p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              margin: "15px",
              width: "450px",
            }}
          >
            <h2 style={{ textDecoration: "underline" }}>Fast and Easy Flow:</h2>
            <p>- Application runs very smoothly even with graho interactions</p>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",

              marginRight: "auto",
              marginLeft: "15px",
              marginTop: "15px",
              width: "450px",
            }}
          >
            <h2 style={{ textDecoration: "underline" }}>
              Saving Research For Further Analysis:
            </h2>
            <p>
              - Bookmarking feature allows for easy revisiting of previous
              analysis
              <br />
              - Also allows us to compare changes in data developments
              <br />- Provides insight into other highly visited research
              findings of other researchers
            </p>
          </div>
        </div>
      </Section>
      <Section title="Conclusion" shade>
        <p>
          We are pleased to observe that the overall consensus received from
          these test users was one filled with positivity and enthusiasm for the
          application developed. We aim to utilise both the positive and
          constructive feedback obtained to further improve the application into
          a more effective and comprehensive application for researchers.
        </p>
        <br />
        <p>
          We are also very pleased to say that our client is also extremely
          satisfied with the application developed with all initial requirements
          being achieved.
        </p>
      </Section>
    </>
  );
};

export default Testing;
