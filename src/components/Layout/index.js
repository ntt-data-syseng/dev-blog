import React from "react";
import { GlobalStyles } from "twin.macro";
import Nav from "../Nav";

const links = [
  "Requirements",
  "Research",
  "UI Design",
  "System Design",
  "Implementation",
  "Testing",
  "Evaluation",
  "Blog",
  "Appendices",
];

const index = ({ children }) => {
  return (
    <>
      <GlobalStyles />
      <div style={{ backgroundColor: "#121B3A" }}>
        <Nav links={links} />
        {children}
      </div>
    </>
  );
};

export default index;
