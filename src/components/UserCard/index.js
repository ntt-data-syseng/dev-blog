import React from "react";
import { LinkedinFilled, MailFilled, GithubFilled } from "@ant-design/icons";
import tw, { styled } from "twin.macro";

const Container = styled.div`
  ${tw`w-full p-2 flex flex-col flex-grow flex-shrink shadow-lg rounded`}
`;

const ActionButton = tw.div`lg:mx-0 hover:underline text-gray-800 font-extrabold rounded my-2 py-2 px-6 shadow-lg rounded cursor-pointer

`;

const UserCard = ({ 
  name,
  image,
  email,
  github,
  linkedIn,
  content,
  roles 
}) => {
  return (
    <Container>
      <div 
      className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden"
      style={{ padding: '10px 5px 0px 5px' }}
      >
        <div className="flex flex-wrap no-underline hover:no-underline">
          <div className="flex items-center">
            <p className="w-full text-gray-600 font-bold text-lg px-4">
              {name}
            </p>
            <div className="rounded-full h-11 w-14 overflow-hidden">
              <img className="w-full h-full" src={image} />
            </div>
          </div>
          <p className="text-gray-600 text-base px-4 mb-5 my-4">{content}</p>
          <p className="text-gray-600 text-base px-4 mb-5 my-4">Roles: {roles}</p>
        </div>
      </div>

      <div 
      className="flex flex-row justify-center md:w-full md:justify-center space-x-5"
      style={{ paddingBottom: '10px'}}
      >
        <ActionButton href={linkedIn}>
          <a href={linkedIn}>
            <LinkedinFilled />
          </a>
        </ActionButton>
        <ActionButton>
          <a href={github}>
            <GithubFilled />
          </a>
        </ActionButton>

        <ActionButton>
          <a href={`mailto:${email}`}>
            <MailFilled />
          </a>
        </ActionButton>
      </div>
    </Container>
  );
};

export default UserCard;
