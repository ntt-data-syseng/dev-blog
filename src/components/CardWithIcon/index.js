import React from "react";
import { AlertFilled } from "@ant-design/icons";

const index = ({ content, title }) => {
  return (
    <div
      className="rounded-xl shadow-lg flex flex-col py-4 px-8"
      style={{ minWidth: "100px", maxWidth: "300px", height: "300px", padding: '20px' }}
    >
      <div>
        <div className="bg-blue-600 rounded-lg inline-block py-4 px-6 shadow-lg my-2">
          <AlertFilled style={{ color: "white", fontSize: 18 }} />
        </div>
      </div>
      <div>
        <h1 className="font-bold text-2xl my-2">{title}</h1>
      </div>
      <div className="w-full">
        <p className="break-words text-gray-900">{content}</p>
      </div>
    </div>
  );
};

export default index;
