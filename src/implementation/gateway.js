import React from "react";

import Figure from "../components/Figure";
import One from "../images/implementation/gateway/one.png";
import Two from "../images/implementation/gateway/two.png";
import Three from "../images/implementation/gateway/three.png";

const gateway = () => {
  return (
    <>
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Service Overview</h1>
      <p>
        This repository is arguably the most important as it links all aspects
        of the application together. It’s role is simple, to map the appropriate
        requests to each backend service. Every time a request is made the
        server is pings ntt-auth service to check the content of the cookies if
        the credentials check out the information of the user is populated in
        the req headers. Hence the backend receives the credentials required
        from the gateway rather than directly interacting with the other
        services, allowing for a faster more efficient service. This also
        removes the act of performing authentication from inside each services,
        essentially decoupling this process from other services and allowing
        them to perform their core tasks.
        <br />
        <br />
      </p>
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>The Code</h1>
      <p>
        The description above can be seen implemented in figure 1:
        <br />
      </p>
      <div className="flex flex-row space-x-12">
        <Figure img={One} figureContent="Figure 1" />
      </div>
      <br />
      <br />
      <p>
        Proxies were also set up to forward requests to the backend services, to
        speed the process and enhance security, seen implemented in figure 2:
        <br />
      </p>
      <div className="flex flex-row space-x-12">
        <Figure img={Two} figureContent="Figure 2" />
      </div>
      <br />
      <br />

      <p>
        Finally, routes were set up enabling the graph and authentication to be
        hosted under the same domain to ease the process of calling upon the two
        services for the frontend as seen below:
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Three} figureContent="Figure 3" />
      </div>

      <br />
      <br />
    </>
  );
};

export default gateway;
