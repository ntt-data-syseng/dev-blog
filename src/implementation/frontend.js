import React from "react";

import Typescript from "../images/implementation/frontend/typescript.png";
import TypescriptModels from "../images/implementation/frontend/typecriptModels.png";
import AuthActions from "../images/implementation/frontend/authActions.png";
import FrontendState from "../images/implementation/frontend/applicationState.png";
import AuthActionCreator from "../images/implementation/frontend/authActionCreator.png";
import ReduxApplication from "../images/implementation/frontend/reduxApplication.png";
import ReducersExample from "../images/implementation/frontend/reducers.png";
import CombineReducers from "../images/implementation/frontend/combineReducers.png";
import SelectorsSample from "../images/implementation/frontend/selectors.png";
import UsageSelectors from "../images/implementation/frontend/usageSelectors.png";
import GraphDataTypeConversion from "../images/implementation/frontend/graphDatatypeConversion.png";
import UtilGraph from "../images/implementation/frontend/utilGraph.png";
import GraphResult from "../images/implementation/frontend/graphResult.png";

import Algo from "../images/implementation/frontend/algo.png";
import Figure from "../components/Figure";

const frontend = () => {
  return (
    <>
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Service Overview</h1>
      <p>
        The frontend service is the user interface that the user will interact
        with our application. Its responsibilities are to handle user's
        selection of the virus taxonomies, receives the responses of the
        analysis from the backend service and display them in a bloom graph. The
        graph has to be interactive so that the user could perform subset
        operations such as hiding common nodes and viruses to assist them in
        their evalution of the task at hand.
        <br />
        <br />
        The foundation of this service is the usage of the NextJS library based
        on React which allows us to build interactive user interfaces. We used
        typescript to alleviate the short comings of Javascript which is a
        dynamically typed language to ensure integrity of properties and methods
        when they are passed around in our application. It also provides type
        checking during the compilation when it transpiles typescript to normal
        javascript and let us know if there any incorrect usage of the code.
      </p>

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={Typescript}
            figureContent="Figure 1: Type checking in Typescript"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={ReduxApplication}
            figureContent="Figure 2: Overview of the flow of action in our Redux powered application"
          />
        </div>
      </div>

      <br />
      <br />
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
        Frontend State Management
      </h1>
      <p>
        Due to the possible large size of the information that is returned and
        information we have to keep track of we need to have a state management
        system. Some analysis contains a lot of nodes and information that is
        returned, an example of this can be seen in the NTT-GRAPH implementation
        where an example highlight is shown. The key information that we have to
        keep track of are the current virus taxonomies to populate the options
        list, the analysis results generated from the backend that we have to
        transform into appropriate format for graph display and the user's
        current authentication status. For this we opted to use Redux with
        supplmentary library such as typesafe-actions to create typescript type
        compliant action. Reselect library is also utilised to help reduce
        complexity to select sub slices of our application state. In our
        application we also defined types to ensure type safety across different
        section of our codes stored into the models folder. An example of a type
        for the graph function is highlighted in the figure below.
        <br />
        <br />
      </p>

      <p>
        An overview of the application's redux state that we have implemented is
        outlined below in the figure. The items are that are highlighted in blue
        are our custom types specific to our application.
      </p>
      <br />
      <br />
      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={FrontendState}
            figureContent="Figure 3: Overview of the Implemented Redux State of the application"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={TypescriptModels}
            figureContent="Figure 4: Snippet of types implemented inside our application"
          />
        </div>
      </div>

      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Action Creators</h1>
      <p>
        To easily mutate and handle actions that our user made we organise these
        possible actions the user can take by creating action creators. Action
        creators and action types to be used in the the redux reducer. Figure
        below illustrates this.... From the actions that we have defined with
        type-safe actions library, we utilised the async type action to create
        typed version actions for each case that could occur on an asynchronous
        task. In the context of our application, the asynchronous task is a web
        request to the backend to query for information. The three states that
        the action could be are requesting, success or failure and these cases
        are illustrated in figure [number] by passing a string name for each
        case in the respective order(request begin, success, failed) as
        mentioned.
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={AuthActions}
            figureContent="Figure 5: Actions created with Type-Safe library for graph requests"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={AuthActionCreator}
            figureContent="Figure 6: Snippet of Action Creator for Graph actions"
          />
        </div>
      </div>

      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Reducers</h1>
      <p>
        To handle dispatched actions the reducer acts as a funnel that handles
        each action cases. It receives the previous state and the current action
        with its payload as the input. Its role is to return the new version of
        the application's state based on the payload provided. The figure below
        shows the implemented reducer for the graph actions that was dispatched
        by our action creators. Using switch cases the handling of each type is
        done methodically, for more complicated mutation of the state utility
        functions, of which the logic is omitted from the figure, is used.
        <br />
        <br />
        Instead of having all the reducers' logic in one file, the application
        has multiple reducers each handling a small subsection of the
        application's state as has been illustrated in the overview figure
        above. This helps with organising the project, developing and testing
        the it. Redux provides a combineReducers() method to combine these small
        reducers into one big reducer and this is shown in figure[number] where
        we combine all the other small reducers each handling their own slice of
        the state into one.
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={ReducersExample}
            figureContent="Figure 7: Reducer Example for Graph state"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={CombineReducers}
            figureContent="Figure 8: Combination of Reducers in the application"
          />
        </div>
      </div>

      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Selectors</h1>
      <p>
        To organise the functio nof getting the specific slice of state that is
        required in each section of our application. Each reducer's slice is
        paired with a group selectors that in it that has been implemented ,
        using the reselect library, to get relevant data per page. An
        implementation for the selectors can be seen in figure[] where we
        illustrate building selectors from other small selectors to do complex
        queries on the state's slice.
        <br />
        <br />
        The usage of these selectors help us to create reusable and encapsulated
        way to access the application's state data to be used to power the user
        interface of our application. Namely in the context of our application
        the existence of these encapsulation help us to prepare data
        appropriately before passing the data to the graph generation function.
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={SelectorsSample}
            figureContent="Figure 9: Graph Selectors Snippet"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={UsageSelectors}
            figureContent="Figure 10: Usage of Selectors for Relevant Information"
          />
        </div>
      </div>
      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
        Graph Data Preparation
      </h1>
      <p>
        The graph rendering library that we have utilised for this project is G6
        from ANTV. The format of the data it needs are as illustrated in figure
        [number]. The data currently in the state has to be prepared, as shown
        from the data structure of the redux store from above. As such there is
        a file called utils inside the graph store that handles this. Its
        responsibilities are to convert the Analysis data into usable for the
        graph library. A short algorithm is shown in figure [number] below.
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={GraphDataTypeConversion}
            figureContent="Figure 11: Conversion Problem"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={Algo}
            figureContent="Figure 12: Outline of Code Logic to Handle Data"
          />
        </div>
      </div>
      <br />
      <br />

      <p>
        The concrete implementation of the algorithm to convert our current
        Analysis datatype to the data type that the library accepts can be seen
        below in figure <b>13</b>. In addition, to the conversion we also have
        to implement labelling function, logic for colouring the common and
        non-common nodes. In addition, based on the amount of nodes and whether
        there are commonality between the selection we also have to manually
        calculate the positions that these nodes will be on the graph canvas.
        These are what the prepeareCommonNodes and prepareEdges are doing to
        ensure a great layout for the user to interact with the chart. These
        utility function is then used inside the graph's reducer selectors
        function so that when the component gets the data it is already prepared
        to be passed to the graph library. Figure <b>14</b> shows the out come
        of this.
      </p>

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={UtilGraph}
            figureContent="Figure 13: Concrete Implementation of Data Handling Logic From Figure 12"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={GraphResult}
            figureContent="Figure 14: Overview of the Implemented Redux State of the application"
          />
        </div>
      </div>
    </>
  );
};

export default frontend;
