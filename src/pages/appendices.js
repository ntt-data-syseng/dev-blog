import React from "react";
import PageTitle from "../components/PageTitle";
import Section from "../components/Section";
import YouTubeEmbed from "../components/YouTubeEmbed";

const Appendices = () => {
  return (
    <>
      <PageTitle title="Appendices" />
      <Section title="Deployment Manual">
        <p>
          We have created a video tutorial to assist the deployment of our
          application. The key steps have also been summarised in text below.
        </p>
        <br />
        <YouTubeEmbed embedId="l1HbnhCwm8M" />
        <br />
        <p>
          1. Create an Azure Virtual Machine and allow the inbound port of 80
          <br />
          2. Follow the Docker installation on Linux Guide to instal docker
          <br />
          3. Follow the Docker-compose guide to install docker-compose on linux
          <br />
          4. Give permission to run Docker by running: sudo usermod -aG docker
          azureuser
          <br />
          5. Logout and log back into the virtual machine to allow the
          permissions to update
          <br />
          6. Generate ssh-key: ssh-keygen -t ed25519 -b 4096
          <br />
          7. Copy ssh-key: cat ~/.ssh/id_ed25519.pub
          <br />
          8. Add the copied ssh-key to bitbucket project access key
          <br />
          9. Clone the MVP repo
          <br />
          11. cd into the project's frontend and paste in the .env.production
          file the url of the vm being hosted on
          <br />
          12. open the .env file in the mvp project and paste in the production
          url the url of the vm being hosted on
          <br />
          13. Run Docker-compose up
        </p>
      </Section>

      <Section title="User Manual" shade>
        <YouTubeEmbed embedId="c5uQR3Z2RHs" />
        <br />
        <p
          style={{
            width: "800px",
          }}
        >
          The user manual for our application can be downloaded
          <a
            href="./userManual.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>
      </Section>

      <Section title="Monthly Videos">
        <p>
          Below are the monthly videos we have uploaded throughout the course of
          the project. These have allowed us to monitor and update our progress,
          ensuring we were on track to successfully meet the initial project
          requirements.
        </p>
        <br />
        <YouTubeEmbed embedId="WnhJze8eSck" />
        <br />
        <YouTubeEmbed embedId="AaJkdhuZ0xo" />
        <br />
        <YouTubeEmbed embedId="_DUDuzXjObQ" />
        <br />
        <YouTubeEmbed embedId="3a03P4GL_7E" />
      </Section>

      <Section title="Legal Issues and Processes" shade>
        <h2 style={{ fontSize: "25px", fontWeight: "800", width: "1000px" }}>
          Legal Statement
        </h2>
        <p>
          The Legal Statement document for our application can be downloaded
          <a
            href="./legalStatement.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>

        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800", width: "1000px" }}>
          End-User Licence Agreement (EULA)
        </h2>
        <p>
          The EULA document for our application can be downloaded
          <a
            href="./eula.pdf"
            target="_blank"
            style={{
              textDecoration: "underline",
              marginLeft: "4px",
            }}
          >
            here
          </a>
        </p>

        <br />

        <h2 style={{ fontSize: "25px", fontWeight: "800", width: "1000px" }}>
          Licence Agreement
        </h2>
        <p>
          Copyright (c) 2021-2022 GitHub Inc.
          <br />
          <br />
          Permission is hereby granted, free of charge, to any person obtaining
          a copy of this software and associated documentation files (the
          "Software"), to deal in the Software without restriction, including
          without limitation the rights to use, copy, modify, merge, publish,
          distribute, sublicence, and/or sell copies of the Software, and to
          permit persons to whom the Software is furnished to do so, subject to
          the following conditions:
          <br />
          <br />
          The above copyright notice and this permission notice shall be
          included in all copies or substantial portions of the Software.
          <br />
          <br />
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
          IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
          CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
          TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
          SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
        </p>
      </Section>
    </>
  );
};

export default Appendices;
