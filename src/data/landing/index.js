export const Problems = [
  {
    title: "Information Overload",
    content:
      "Usual research methods produce excessive data points making it difficult to extract and comb through relevant information.",
  },
  {
    title: "No Visualisation",
    content:
      "No existing centralised platform for which necessary data can be easily obtained along with visual representations.",
  },
  {
    title: "No Collaboration",
    content:
      "Unable to collaborate with other researchers/professors on discoveries",
  },
];
