import React from "react";
import Iframe from "react-iframe";

import Models from "../images/implementation/graph-db/models.png";
import PostgresModels from "../images/implementation/graph-db/postgresModels.png";
import Routes from "../images/implementation/graph-db/routes.png";
import NodeConnect from "../images/implementation/graph-db/node-connect.png";
import PopulateScript from "../images/implementation/graph-db/populate_script.png";
import CreateTaxonomy from "../images/implementation/graph-db/create_taxonomy.png";

import PydanticModels from "../images/implementation/graph-db/pydanticModels.png";
import ResponseSchema from "../images/implementation/graph-db/responseSchema.png";
import AnalyseFunction from "../images/implementation/graph-db/analyseFunction.png";
import GetUnique from "../images/implementation/graph-db/get_unique.png";
import JsonResponse from "../images/implementation/graph-db/jsonResponse.png";
import TaxonomyListing from "../images/implementation/graph-db/taxonomyListing.png";

import Figure from "../components/Figure";

const graph = () => {
  return (
    <>
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Service Overview</h1>
      <p>
        The NTT-GRAPH service is the backend service that performs analysis on
        the dataset that has been cleaned as a prerequisite. It is built using
        FastAPI and utilises both NEO4j and PostgresQL. The intention of this
        service is that it provides a REST API that returns analysis,
        coronaviridae taxonomies and kos in the json format. PostgresQL is used
        to store a counter for each analysis done by the user, to be able to
        identify each set of analysis uniquely an id of each analysis is
        calculated by combining the two taxonomy ids by multiplication. This is
        how we are going to store the analysis with the counter information in
        the postgres database.
        <br />
        <br />
        Neo4J will be used to store the cleaned dataset. There are usually
        multiple viruses that are strains of each other and have common KO, or
        functionality. It would be difficult to model this in a relational
        database as each strain linked by various different KOs appear and
        relation are identified by foreign key.
      </p>

      <br />
      <br />
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Neomodel</h1>
      <p>
        The reason Python was chosen as the main backend to perform analysis on
        the dataset from Neo4j is because of the great support with Neomodel.
        Neomodel is an Object Relational Model(ORM) that allows us to create
        properties of a node and query information about it inside the graph
        data without having to write pure cypher query. However, for more
        advance stuff we still have to create our own cypher query which will be
        encapsulated into the related model. The implemented models that match
        to the node of the NEO4j Database are shown in figure 1 below.
      </p>

      <br />
      <br />
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>SQLAlchemy</h1>
      <p>
        We used SQLAlchemy to model our Postgres Database. We have created two
        tables, which are Bookmarks and Analysis. These models are used to
        persist the user's saved analysis and to store the amount of times the
        analysis has been requested respectively. The structure of the model is
        shown in the code implementation in figure 2.
      </p>
      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={Models}
            figureContent="Figure 1: The Models Created for NEO4j"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={PostgresModels}
            figureContent="Figure 2: Postgres Models created"
          />
        </div>
      </div>

      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Pydantic Models</h1>
      <p>
        We have created Pydantic models which comes with FastAPI to validate the
        incoming reuqest body as shown in figure 3. It allows us to validate
        that the incoming request's body structure is correct and can be reused
        in the response schema as shown in figure 4 where we reused the models
        implemented before, which is used in each route endpoint. This also has
        the added benefit of type-checking in a language is duck-typed.
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={PydanticModels}
            figureContent="Figure 3: Pydantic Models Created"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={ResponseSchema}
            figureContent="Figure 4: Response Schema Created"
          />
        </div>
      </div>

      <br />
      <br />
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Endpoint Routes</h1>
      <p>
        This service has 4 category of endpoints. Firstly, there are two group
        of endpoints KO and Taxonomy which performs normal CRUD functionalities.
        The action routes are the main feature of this service. It is where the
        analysis of the virus taxonomies are created, how the nodes get
        connected with each other, bookmarking the analysis and optaining the
        most used analysis. The endpoints are shown in figure 5 below.
        <br />
        <br />
      </p>

      <br />
      <br />
      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={Routes}
            figureContent="Figure 5: Endpoint Routes of NTT-Graph"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={CreateTaxonomy}
            figureContent="Figure 6: Route to create taxonomy"
          />
        </div>
      </div>

      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Data Population</h1>
      <p>
        To populate the data we utilised the CRUD routes to create the virus
        taxonomy and the ko information from the clean data csv file. The code
        is shown in figure 7, it reads the csv file and perform requests on
        endpoints to create the taxonmy. An example of the endpoint that creates
        a node is shown in figure 6. Once that is completed, we used the
        relations table from the csv to connect the related kos and the taxonomy
        together. This function is highlighted in the figure 8 below. This
        script can be run on every deployment, or simply commented out if we
        chose not to update the current taxonomy and ko information. The
        neomodel has exposed a method has_[model_name] which can create
        relationships between two model.
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={PopulateScript}
            figureContent="Figure 7: Script to populate the data into the service"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={NodeConnect}
            figureContent="Figure 8: Connecting of Nodes"
          />
        </div>
      </div>

      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
        Performing Analysis
      </h1>
      <p>
        To perform analysis the code snippet in figure 9 is created. We first
        find the common KOs between the two taxonomies. This is simple todo as
        illustrated in the figure below. We did this by performing a simple
        Cypher match query between two provided taxonomy ids and getting the
        nodes between the two of them. We then have to pass this common nodes to
        the function to get Taxonomy with only KOs unique to it. Essentially we
        are using the common nodes as a filter from the KOs returned from the
        query. The custom query function implemented for the Taxonomy node can
        be seen in figure 10. To explain the query, we first find the taxonomy
        from the provided id, get its first neighbour nodes (KO) and filter
        those by the common nodes that were passed in.
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure img={AnalyseFunction} figureContent="Figure 9: Reducers" />
        </div>

        <div className="w-1/2">
          <Figure
            img={GetUnique}
            figureContent="Figure 10: Get Unique Taxonomy with Unique KOs"
          />
        </div>
      </div>

      <br />
      <br />

      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
        Final Result of Analysis
      </h1>
      <p>
        This service core requirement is to return the analysis between two
        taxonomies in a well structured JSON format which is easy to understand
        for use in the frontend. To do this we have to return all the taxonomies
        as shown in the figure 11 so that the user could have the options to
        pick the taxonomies that they want to analyse. Once selected the user
        submits the id of the two taxonomies to the analyse endpoint and in
        figure 12 is the result when you request analysis between two nodes. In
        the data object of the response we have two properties which are the
        nodes and the common_nodes. Nodes are the taxonomy information and has a
        sub-property called unique_nodes if there are any taxonomy that are not
        common between the two taxonomies. The common_nodes are the KO nodes
        that are found in both the taxonomies. From the sample in the figure
        only "T40361" has a unique KO when comparing between "T40069".
      </p>
      <br />
      <br />

      <div className="flex flex-row space-x-12">
        <div className="w-1/2">
          <Figure
            img={TaxonomyListing}
            figureContent="Figure 11: Request for all taxonomies"
          />
        </div>

        <div className="w-1/2">
          <Figure
            img={JsonResponse}
            figureContent="Figure 12: Response Structure"
          />
        </div>
      </div>
      <br />
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
        You can interact with the iframe below of the graph-service through the
        usage of swagger documentation to test out our implementation. If the
        iframe does not load please proceed to
        http://20.213.242.240/api/graph/docs
      </h1>

      <br />
      <div className="border-red">
        <Iframe
          url="http://20.213.242.240/api/graph/docs"
          allowtransparency="true"
          style={{ background: "red" }}
          width="100%"
          height="450px"
          className="border-red"
          display="initial"
          position="relative"
        />
      </div>
    </>
  );
};

export default graph;
