const thomaskeys = [
  {
    key: "Tell me about yourself, what is your job and experience?",
    value: `I am currently a PhD student studying gene clusters 
    at University College London. More specifically I 
      specialise in researching the gene structure of different strains of the coronavirus to manufacture a durable, 
      effective vaccine.`,
  },
  {
    key: "Is there an existing platform to identify similarities and differences between gene clusters within organisms? If not, do you believe it’s a good idea?",
    value: `Unfortunately, 
    there isn’t currently a program that enables you to do this. I feel it would be beneficial for 
    researchers in particular. At the moment all this data is stored in ‘flat files’ 
    and hence we have to manually open multiple files and switch between them to make effective comparisons. However, this newly proposed software has made me incredibly 
    excited as I can already see that this will save me time and will also enable a more efficient comparison of organisms and reduce the error margin as all the information 
    can be ordered and viewed at once.`,
  },
  {
    key: "Do you believe this software will benefit the general public as well?",
    value: `
    Definitely, if us researchers are able to effectively compare organisms, we can learn more about their similarities and differences and in many cases 
    this means we are able to manufacture much needed medicine. 
    I know for certain, comparing different strains of the coronavirus will help us procure a more effective and stronger vaccine.`,
  },
];

const rachelkeys = [
  {
    key: "Tell me about yourself, what is your job and experience?",
    value: `I have been a professor at UCL for over 10 years and I’m currently doing research in common functionalities
    between genes.
    `,
  },
  {
    key: "What list of features do you desire for the product? If so, why?",
    value: `I would like a visual representation of that data and have the ability to drill down on each node into further detail as this would aid my teaching. 
    I also think the convenience to
    jot down notes and generate a report would be beneficial for most students and even for myself as a researcher.`,
  },
  {
    key: "What would you say is the most important criteria for this product to be successful?",
    value: `
    I would say a simple user interface and clear functionality is pivotal - I am tentative about using/learning new complex technology that is difficult to grasp. 
    The analysis generated should also not be too time expensive. `,
  },
  {
    key: "What would you say is the ideal workflow for this product?",
    value: `Once on the platform, I would want the ability to select multiple organisms to generate the visual clusters. I should then be able to 
    select the nodes to view more information and there should be an option to take notes on this analysis. 
    Exporting a summary report is vital, preferably at the click of a button rather than a complex mechanism.`,
  },
];

const interviews = {
  "Interview with Thomas": thomaskeys,
  "Interview with Rachel": rachelkeys,
};

export default interviews;
