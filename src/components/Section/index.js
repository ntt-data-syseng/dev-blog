import React from "react";
import tw, { styled } from "twin.macro";

// const Container = tw.div`${(props) =>
//   props.shade ? `bg-gray-100` : `bg-white`} `;

const Container = styled.section`
  ${(props) =>
    props.shade ? tw`bg-gray-100` : tw`bg-white`}${tw`border-b py-8`}
`;

const index = ({ title, subtitle, children, shade = false }) => {
  return (
    <Container shade={shade}>
      <div className="container max-w-5xl mx-auto m-8 px-4">
        <div className="mx-auto flex justify-center flex-wrap">
          <h2 className="w-full my-2 text-5xl font-black leading-tight text-center text-gray-800 ">
            {title}
          </h2>
          <h2 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">
            {subtitle}
          </h2>
          <div className="px-4">{children}</div>
        </div>
      </div>
    </Container>
  );
};

export default index;
