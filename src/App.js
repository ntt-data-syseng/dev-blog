import { MemoryRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./pages/index";

import Layout from "./components/Layout";
import RequirementsPage from "./pages/requirements";
import ResearchPage from "./pages/research";
import UIDesignPage from "./pages/uidesign"
import SystemDesignPage from "./pages/systemdesign"
import TestingPage from "./pages/testing";
import EvaluationPage from "./pages/evaluation";
import ImplementationPage from './pages/implementation'
import AppendicesPage from "./pages/appendices";
import ErrorPage from "./pages/404";
import Blog from "./pages/blog"

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/requirements" element={<RequirementsPage />} />
          <Route path="/research" element={<ResearchPage />} />
          <Route path="/uidesign" element={<UIDesignPage />} />
          <Route path="/systemdesign" element={<SystemDesignPage />} />
          <Route path="/implementation" element={<ImplementationPage />} />
          <Route path="/testing" element={<TestingPage />} />
          <Route path="/evaluation" element={<EvaluationPage />} />
          <Route path="/appendices" element={<AppendicesPage />} />
          <Route path="/blog" element={<Blog />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Layout>
    </Router>
  );
}
export default App;
