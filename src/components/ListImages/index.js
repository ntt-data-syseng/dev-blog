import React from "react";

const Index = ({ data }) => (
  <div className="flex flex-row flex-wrap">
    {data.map((image) => (
      <div
        key={image}
        className="md:w-1/2 p-4 px-6"
        style={{ 
          aspectRatio: 3/2 }}
      >
        <img src={image} className="w-full h-full shadow-lg rounded" />
      </div>
    ))}
  </div>
);

export default Index;
