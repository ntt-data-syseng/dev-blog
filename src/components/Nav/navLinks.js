import React from "react";
import tw from "twin.macro";
import { Link } from "react-router-dom";

const Container = tw.div`w-full flex-grow lg:flex lg:items-center lg:w-auto mt-2 lg:mt-0 text-black z-20`;

const ListItem = ({ name }) => (
  <li className="mr-3">
    <div className="inline-block py-2 px-4 text-black font-bold no-underline">
      {name}
    </div>
  </li>
);

const NavLinks = ({ names }) => {
  return (
    <Container>
      <ul className="list-reset lg:flex justify-end flex-1 items-center">
        {names.map((name) => (
          <Link key={name} to={`/${name.replace(" ", "").toLowerCase()}`}>
            <ListItem name={name} />
          </Link>
        ))}
      </ul>
    </Container>
  );
};

export default NavLinks;
