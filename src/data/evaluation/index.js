export const criticalEvaluationTabs = {
  "User Experience": [
    "The team believes a clean user interface has been designed to ensure a productive and smooth user experience. \
        The website is self-explanatory, meaning the user needs little to no help to use the application. The ‘Generate graph’ \
        button is large and clearly marked, the login button is located on the top left of the page as seen in most other applications, \
        the toolbar button contrasts with the background to standout. The nodes have also been colour coded. There is also feedback \
        when the user completes any action, for example when the user generates a pdf, a message is sent to confirm with the user \
        that their action has been acknowledged, in addition when there is no graph generated the buttons for sharing, pdf and bookmarking \
        while present are rendered unresponsive, hinting to the user that a graph must be generated.",
    "If time permitted, \
        to further the user experience the team could have provided feedback as well, hence when these buttons are pressed a \
        message is send to the user directing them to first generate a graph. Also, the team could’ve implemented the toolbar \
        more effectively, for example hovering over the button bring up the options. However, overall, the experience is \
        smooth, responsive, and clean for the user, and our user acceptance testing shows this.",
  ],
  Functionality: [
    "The team has always focused on the key design target which is aiding researchers in understating different organisms.",
    "Implementation for all requirements provided by the client such as the bloom graph and PDF generation was successful. \
        The team also implemented extra features such as the sharing capability, the bookmarking, the authentication system \
        and the filtering. There are no known bugs and the application runs smoothly.",
  ],
  "Stability-Efficiency": [
    "We identify our project to be sufficiently stable and efficient due to the project development approach and procedure \
        we followed. Through separating the implementation of core components of our project into different repositories, we were \
        able to isolate these functionalities and ensure they are efficiently operating through thorough testing, prior to \
        combining the whole project. This along with the success in which our application has passed our tests has provided \
        us with great confidence that we have developed a robust application that satisfies the initial requirements set out by our client.",
  ],
  Compatibility: [
    "The web application has been deployed. It’s clear this application is compatible on all browsers while also \
        automatically reformatting depending on the size of the window, meaning it is compatible with all laptop and \
        computer screen sizes. However, reducing the window after a certain threshold does not yield the webpage usable and the application is not compatible with smartphone browsers.",
  ],
  Maintainability: [
    "We were able to efficiently modularise our project through separating each core functionality of the project into separate \
        repositories. This allowed us to isolate these different components ensuring that they are sufficiently operating and tested \
        prior to combining them into our mvp project. This also eased the debugging process as it was easier to identify error points \
        in the code as the code base would be smaller for each individual repository.",
  ],
  "Project Management": [
    "The project has been successfully managed throughout the course of the academic year through several mechanisms. \
        We declared clear project roles at the beginning of the project to ensure each team member knew their responsibilities \
        and would remain accountable for them. In addition to this, we established a clear communication stream through a \
        Whatsapp group chat for general project discussions as well as weekly meetings for more thorough updates. Moreover, \
        we ensured that our client was fully updated with our progress and any difficulties we were facing through the entirety \
        of the project. This also allowed us to fully understand the different requirements they had in place and work towards \
        successfully achieving them.",
  ],
};
