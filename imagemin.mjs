import imagemin from "imagemin";
import imageminJpegtran from "imagemin-jpegtran";
import imageminPngquant from "imagemin-pngquant";

// replace build/images with your own path if needed
const files = await imagemin(["./build/static/media/*.{jpg,png,jpeg}"], {
  destination: "./build/static/media/",
  plugins: [
    imageminJpegtran(),
    imageminPngquant({
      quality: [0.6, 0.8],
    }),
  ],
});

console.log(files);
