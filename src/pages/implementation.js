import React, { useState } from "react";
import PageTitle from "../components/PageTitle";
import Section from "../components/Section";

import One from "../images/implementation/script/table.png";
import Two from "../images/implementation/script/code1.png";
import Three from "../images/implementation/script/three.png";
import Four from "../images/implementation/script/four.png";
import Five from "../images/implementation/script/five.png";
import Six from "../images/implementation/script/six.png";

import Frontend from "../implementation/frontend";
import Graph from "../implementation/graph";
import Auth from "../implementation/auth";
import Pdf from "../implementation/pdf";
import Gateway from "../implementation/gateway";

const SystemDesign = () => {
  const [currentTab, setCurrentTab] = useState("frontend");

  const content = {
    frontend: <Frontend />,
    graph: <Graph />,
    auth: <Auth />,
    gateway: <Gateway />,
    pdf: <Pdf />,
  };

  return (
    <>
      <PageTitle title="Project Implementation" />

      <Section title="Overview" shade>
        <p>
          To manage and deliver on all our key requirements for the project we
          have splitted our project into self contained repositories, each
          handling a set of related features for our application. Each of these
          subprojects can then be developed concurrently with other features
          whilst reducing merge conflicts from developers touching the same
          file. Furthermore, since eases the testing process as we can testing a
          subset of app functionalities in each repository instead of one big
          test suite for the application. From here on out these subprojects
          will be refer to as services and our prototype has 6 of those that we
          have developed.
        </p>
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>Data Cleaning</h1>
        <p>
          It was decided that python would be used to write the script to
          extract the data, due to it’s clean and agile syntax while also being
          a language that’s fast, well documented and has a lot of libraries
          especially regarding reading and storing data.
          <br />
          <br />
          There are only eight strains of the virus that are stated in the
          specification; hence it was decided to use the KEGG API to display
          only the data that was required. The team required three separate CSV
          files to act as the database. The first displaying the relevant
          taxonomy numbers, the next displaying the KO numbers and finally the
          last displaying the relationship between each taxonomy and its
          relevant KO’s. It was decided the data would be stored in tables
          similar to the ones below:
        </p>
        <br />
        <img src={One} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          The “list” and “get” operations were used to display the data
          required, and the data was narrowed down by specifying to the API the
          strains of the virus we were interested in. This was done by listing
          the all the vg numbers on the database, hence narrowing the data
          stored on KEGG to just the coronavirus data. The code for this can be
          seen below:
        </p>
        <br />
        <img src={Two} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          The team was able to connect to the KEGG API using the ‘requests’
          library on python. It was decided to use this library as it allows you
          to send HTTP/1.1 requests extremely easily. There’s no need to
          manually add query strings to your URLs, or to form-encode your PUT
          and POST data.
          <br />
          <br />
          The ‘get’ operation from the API along with the vg numbers stored were
          then used to find the taxonomy and KO numbers. as seen below when the
          following request is sent:
        </p>
        <br />
        <p>http://rest.kegg.jp/get/vg:918758</p>
        <br />
        <p>
          The following data is displayed including the KO and taxonomy numbers
          (which are circled):
        </p>
        <br />
        <img src={Three} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          After all this data was displayed the team implemented regexes to
          split and find the data that was relevant. For example [pattern =
          "TAX:[0-9]+”] was used to derive the taxonomy number. The ‘re’ library
          was implemented to here as it provides regular expression matching
          operations similar to those found in Perl.
          <br />
          <br />
          This TAX number was then used to derive the KO number, again
          extracting this information using regexes. This data was then written
          to the first CSV file which stored the relationships between the
          taxonomies and the KO’s. The code bellow shows this process:
        </p>
        <br />
        <img src={Four} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          Now that the relationships had been successfully extracted the team
          moved onto extracting the information regarding the taxonomy and KO’s.
          To extract information about the taxonomies the request below was sent
          to the KEGG API.
        </p>
        <br />
        <p>http://rest.kegg.jp/get/gn:TAXNUMBER</p>
        <br />
        <p>The code below shows how we did this:</p>
        <br />
        <img src={Five} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          Regexes were used again to extract the information required. The data
          extracted included the tax entry id, tax id, name and further
          miscellaneous. This was all stored in a second CSV file. The code for
          this can be seen below:
        </p>
        <br />
        <img src={Six} style={{ margin: "auto", marginTop: "20px" }} />
        <br />
        <p>
          The exact same process was carried out to extract the KO data.
          <br />
          <br />
          Due to the amount of the data that needed to be searched through the
          script took hours to run, however upon the successful completion of
          the storage of the data the team was ready to move onto the next
          phase. Inserting this data into Neo4J to start forming bloom graphs.
          <br />
          <br />
          The “errno” library was implemented for error handling also as it
          makes available standard errno system symbols. The value of each
          symbol is the corresponding integer value. The names are very
          inclusive, and made the team’s job easier.
          <br />
          <br />
          The cleaned data is kept in the Cleaned-Data repo.
        </p>
      </Section>
      <Section title={"Services Implementation"}>
        <br />
        <br />
        <h1 className="font-bold">
          Our Application are then splitted into 6 submodules each handling
          their own respective tasks. Each of the submodule's implementation are
          highlighted in each tab below.
        </h1>
        <div
          style={{
            flex: "column",
            display: "flex",
            width: "100%",
            margin: "20px auto",
            textAlign: "center",
          }}
        >
          <div className="w-1/5">
            <h1
              style={{ fontSize: "17px", fontWeight: "800" }}
              className={currentTab === "frontend" ? "underline" : ""}
              onClick={() => setCurrentTab("frontend")}
            >
              FRONTEND
            </h1>
          </div>
          <div className="w-1/5">
            <h1
              style={{ fontSize: "17px", fontWeight: "800" }}
              className={currentTab === "graph" ? "underline" : ""}
              onClick={() => setCurrentTab("graph")}
            >
              NTT-GRAPH
            </h1>
          </div>
          <div className="w-1/5">
            <h1
              style={{ fontSize: "17px", fontWeight: "800" }}
              className={currentTab === "auth" ? "underline" : ""}
              onClick={() => setCurrentTab("auth")}
            >
              NTT-AUTH
            </h1>
          </div>

          <div className="w-1/5">
            <h1
              style={{ fontSize: "17px", fontWeight: "800" }}
              className={currentTab === "gateway" ? "underline" : ""}
              onClick={() => setCurrentTab("gateway")}
            >
              NTT-GATEWAY
            </h1>
          </div>
          <div className="w-1/5">
            <h1
              style={{ fontSize: "17px", fontWeight: "800" }}
              className={currentTab === "pdf" ? "underline" : ""}
              onClick={() => setCurrentTab("pdf")}
            >
              NTT-PDF
            </h1>
          </div>
        </div>
        <br />
        <br />
        {content[currentTab]}
      </Section>
    </>
  );
};

export default SystemDesign;
