import React from "react";
import PageTitle from "../components/PageTitle";
import TabContent from "../components/TabContent";
import ListImages from "../components/ListImages";
import Section from "../components/Section";
import { personas } from "../data/hci/data";
import { styled } from "twin.macro";
import UseCase from "../images/requirements/usecase.png";
import UseCaseListMain from "../images/requirements/useCaseListMain.png";
import UC1 from "../images/requirements//UC1.png";
import UC2 from "../images/requirements//UC2.png";
import UC3 from "../images/requirements//UC3.png";
import UC4 from "../images/requirements//UC4.png";
import MoscowList from "../images/requirements/moscowList.png";
import MoscowNonFunctional from "../images/requirements/moscowNonFunctional.png";

import moscowRefined from "../data/requirements/moscow";
import interviews from "../data/requirements/interview";

const Test = styled.div`
  min-height: 400px;
  @media only screen (min-width: 640px) {
    min-height: 250px;
  }
`;

const Requirements = () => {
  return (
    <>
      <PageTitle title="Discovering Requirements" />
      <Section title="Project Background" shade>
        <p className="break-words">
          NTT DATA is a trusted global innovator of IT and business services who
          are part of the NTT Group and are headquartered in Tokyo. The company
          helps clients transform through consulting, industry solutions,
          business process services, IT modernization and managed services to
          create new paradigms and values, which help contribute to a more
          affluent and harmonious society.
        </p>
        <br />
        <p>
          As part of this, following the impact COVID-19 has had around the
          globe, NTT DATA are determined to enable the return to normality by
          producing an efficient vaccine. To aid this goal, the company wishes
          to provide a software that researchers can utilize to compare, analyse
          and study different strains of the coronavirus to better understand
          this deadly virus.
        </p>
      </Section>
      <Section title="Client Requirements">
        <div className="mb-12">
          <p className="break-words whitespace-pre-wrap">
            In order to further understand the requirements of the project, a
            meeting was arranged with the client. During this meeting, the
            client provided us with a list of requirements divided into the main
            goals, key issues and optional requirements. In the following
            meetings, any ambiguities were clarified and the requirements list
            was updated where needed. Finally we finalised a MosCow list
            combining the project requirements. This was then presented to the
            client for verification. Below is a list of example questions that
            were posed during the meetings:
          </p>
        </div>

        <ul className="list-decimal list-inside space-y-4">
          <li>Who will be the main users of the program/software?</li>
          <li>What are the goals of the users?</li>
          <li>
            What would you like the product to be delivered? (i.e website,
            desktop application or mobile application?)
          </li>
          <li>
            Where would the product be used and under what circumstances? (i.e
            the place, time and situation?)
          </li>
          <li>
            What strains of the coronavirus are required for the database?
          </li>
        </ul>
      </Section>
      <Section title="Project Goals" shade>
        <p className="break-words whitespace-pre-wrap">
          Our project aims to develop a web application purposed for visual
          analysis of common gene clusters in disease-causing viruses in the
          Coronaviridae category. This will be achieved through presenting
          graphical information using Neo4j, in the form of bloom graphs. Thus,
          directly presenting the similarities and differences between the gene
          clusters of the different organisms. This will ultimately translate
          the large inconsistent Kegg dataset into a more digestible format for
          researchers, aiding their research process. The application will allow
          users to generate pdf reports of their findings which can then be
          shared with colleagues for further analysis and collaboration.
        </p>
      </Section>
      <Section title="User Interviews">
        <p className="break-words whitespace-pre-wrap mb-16">
          To further gather requirements for our application, we decided to
          conduct surveys with individuals from our identified customer
          segments. These interviews were designed to gain a better insight into
          the main pain points for researchers in this field and what solutions
          they would benefit from to aid their analysis process. A summary of
          the questions asked along with each interviewee's response is provided
          below.
        </p>
        <TabContent data={interviews} defaultOption="Interview with Thomas" />
        <p className="break-words whitespace-pre-wrap mt-16">
          Utilising this newly discovered information, we were able to reassess
          our application proposal and further focus on certain specific
          requirements. It was clear that the main concept appreciated by the
          interviewees was the idea of a simplistic platform by which excessive
          and complex information could be presented graphically. From this, we
          were able to hone in on the core value features of the application.
        </p>
      </Section>
      <Section title="Personas" shade>
        <ListImages data={personas} />
      </Section>
      <Section title="Use Case Diagram">
        <div className=" p-4 px-6" style={{ height: "500px" }}>
          <img
            src={UseCase}
            className="w-full h-full shadow-lg rounded"
            alt="usecase illustration"
          />
        </div>
      </Section>
      <Section title="Use Case List" shade>
        <div className=" p-4 px-6">
          <img
            src={UseCaseListMain}
            className="w-full h-full shadow-lg rounded"
            style={{ marginBottom: "20px", marginTop: "20px" }}
            alt="usecase illustration"
          />
          <img
            src={UC1}
            className="w-full h-full shadow-lg rounded"
            style={{ marginBottom: "20px", marginTop: "20px" }}
            alt="usecase illustration"
          />
          <img
            src={UC2}
            className="w-full h-full shadow-lg rounded"
            style={{ marginBottom: "20px", marginTop: "20px" }}
            alt="usecase illustration"
          />
          <img
            src={UC3}
            className="w-full h-full shadow-lg rounded"
            style={{ marginBottom: "20px", marginTop: "20px" }}
            alt="usecase illustration"
          />
          <img
            src={UC4}
            className="w-full h-full shadow-lg rounded"
            style={{ marginBottom: "20px", marginTop: "20px" }}
            alt="usecase illustration"
          />
        </div>
      </Section>
      <Section title="MosCow List">
        <div>
          <img src={MoscowList} style={{ margin: "0 auto" }} />
        </div>
        <br />
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>Non Functional</h1>
        <br />
        <br />
        <img src={MoscowNonFunctional} />
      </Section>
      x
    </>
  );
};

export default Requirements;
