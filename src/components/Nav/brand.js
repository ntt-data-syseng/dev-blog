import React from "react";
import BrandImage from "../../images/logo/logo.png";
import tw from "twin.macro";

const Container = tw.div`pl-4 flex items-center text-black`;
const BrandContainer = tw.div`text-white no-underline hover:no-underline font-bold text-2xl lg:text-4xl text-black text-blue-800`;

const Brand = ({ brandName }) => (
  <Container>
    <BrandContainer>
      {/* <p>{brandName}</p> */}

      <img src={BrandImage} style={{ width: "80px" }} />
    </BrandContainer>
  </Container>
);

export default Brand;
