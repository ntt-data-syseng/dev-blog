import React from "react";
import Section from "../components/Section";
import ListImages from "../components/ListImages";

import PageTitle from "../components/PageTitle";

import { sketches, figmas } from "../data/hci/data";
import { AlignCenterOutlined } from "@ant-design/icons/lib/icons";

const UIDesign = () => {
  return (
    <>
      <PageTitle title="User Interface Design" />

      <Section title="Design Principles"shade>
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">User Centred</h1>
        <br />
        <p>
        It’s well known the most important design principle is focusing on the user. 
        Good user interfaces enable users to feel like they are in control and comfortable 
        and hence by default need to be easy to use and avoid confusing the user. 
        Hence when prototyping the product, the team ensured that users were always 
        consulted and referred to ensure the design was user-friendly.
        </p>
        <br />
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Unity and Variety</h1>
        <br />
        <p>
        The principle unity operates within a design giving the appearance of oneness or resolution. 
        This ensures no single part is more important than the other. Alex White explains in his book 
        The Elements of Graphic Design, “To achieve visual unity is the main goal of graphic design. 
        When all elements are in agreement, a design is considered unified.” 
        Variety is also important to prevent the features of the application 
        from merging and becoming indistinguishable. An example of unity and variety 
        in our project can be seen by the nodes on the graph. While there 
        is unity in shape to provide a clean professional look, there is variety in size and colour 
        to distinguish different types of nodes from one another.
        </p>
        <br />
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Emphasis</h1>
        <br />
        <p>
        Emphasis is used to focus the viewer’s attention on a certain part of the design. 
        The effect is achieved by changing elements (like colour, shape and size) to make 
        specific parts of a design stand out. This again can be seen in our design. There 
        are different colours of the organism, unique and similar nodes to provide that 
        contrast needed.
        </p>
        <br />
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Visibility</h1>
        <br />
        <p>
        This principle dictates that users need to know what all the options are and know 
        straight away how to access them. The visibility principle is very easy to implement 
        on a web application as a clear menu can be designed to direct and inform the user.
        </p>
        <br />
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Feedback</h1>
        <br />
        <p>
        It’s well known saying that every action needs a reaction, and it’s no different in UI design. 
        Once a user wants to carry out a function, the user needs to know that it's being processed 
        or complete, for example when searching the web, we see a loading sign. In our project this 
        was easy to implement as the graph generation is instantaneous, hence the user sees the reaction 
        for their action straight away.
        </p>
        <br />
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Consistency</h1>
        <br />
        <p>
        This principle states that the same action must cause the same reaction every time.
        This can be seen implemented around the project. For example, when generate graph is clicked, 
        the graph will always be generated.
        </p>
        <br />
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Simplicity</h1>
        <br />
        <p>
        Simplicity is a design principle that considers the user's goals and provides the 
        simplest way to achieve those goals. It also has the goal to create a simple and minimal 
        design that makes the user happy requires certain actions. Why is the apple operating 
        system so successful? Because they kept their design simple, clean, and unique. The team 
        have implemented this in the project as well by making sure the design is clean and simple, 
        as seen in the prototype.
        </p>
        <br />
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Tolerance</h1>
        <br />
        <p>
        This principle reduces the cost of mistakes and misuse by users by allowing undoing and redoing to prevent 
        frustration on the user’s side. This can clearly be seen in our design by easily allowing users to generate 
        new graphs in the event the wrong organism has been selected.
        </p>
      </Section>

      <Section title="Sketches">
        <p>
        After discovering requirements and completing the personas/scenarios to further our understanding of the 
        users and their needs, we created multiple sketches to find the most efficient design of the user interface. 
        We then developed our idea and iterated our designs upon showing them to users until we arrived at our final 
        iteration as seen below. 
        </p>
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Final Iteration</h1>
        <br />
        <ListImages data={sketches} />
      </Section>

      <Section title="Figma" shade>
        <p>
        We then constructed a prototype using Figma to further explore the usability of our sketches. The images 
        shown below map out the user experience for the web application.  We then asked the users to demo the 
        prototype and capture data points that we could improve on.
        </p>
        <h1 className="w-full my-2 text-3xl font-semibold leading-tight text-center text-gray-900">Final Iteration</h1>
        <br />
        <ListImages data={figmas} />
      </Section>
    </>
  );
};

export default UIDesign;
