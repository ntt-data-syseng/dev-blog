export const moscow = {
  "Must Haves": [
    "Present graphical information on the similarities and differences between gene clusters",
    "Up to date KEGG and Taxonomy information synchronisation with data website",
    "Functionality to select and further probe a specific node - presenting all it’s necessary data",
    "Ability to move the node of the graph and interact with it to increase usability and understanding of the graph",
    "An option to generate a PDF report on the data produced and analysed",
  ],
  "Should Haves": [
    "The ability to choose a number of different organisms to analyse as opposed to just 2 organisms",
    "Allow for users to comment/make notes on the bloom graph and specific nodes/KO numbers",
  ],
  "Could Have": [
    "A filtering option to isolate unique and similar nodes for the different Taxonomies",
    "The ability to eradicate trivial components of the bloom graph and apply further focus on specific nodes",
    "Provide an authenticatication system to provide greater security on the user’s research",
    "Allows users to save the bloom graphs generated to their account for greater convenience",
    "Share the current graph project with another perso",
  ],
  "Will Not Have": [
    "The ability to solely analyse a single taxonomy as opposed to comparing with one another",
    "The ability to switch between light and dark mode depending on the user preference",
  ],
};

const moscowRefined = {
  "Must Haves": [
    "Present graphical information on the similarities and differences between gene clusters. In the context of our application specifically highlight them by each colours",
    "Up to date KEGG and Taxonomy information synchronisation with data website. Essentially on each deployment of the application we have the option to perform an update",
    "Functionality to select and further probe a specific node - presenting all it’s necessary data. In the context of our application interact with the data that is presented, seeing the genomic pathway and other miscellaneous information",
    "Ability to move the node of the graph and interact with it to increase usability and understanding of the graph. We have to ensure that the generated analysis does not massively dump data on our user, hence we have to present them in small chunks, in our case we had to present a simpler graph presentation and then allow the user to click on each node to view further information",
    "An option to generate a PDF report on the data produced and analysed",
  ],
  "Should Haves": [
    "The ability to choose a number of different organisms to analyse as opposed to just 2 organisms",
    "Allow for users to comment/make notes on the bloom graph and specific nodes/KO numbers",
  ],
  "Could Have": [
    "A filtering option to isolate unique and similar nodes for the different Taxonomies",
    "The ability to eradicate trivial components of the bloom graph and apply further focus on specific nodes",
    "Provide an authenticatication system to provide greater security on the user’s research",
    "Allows users to save the bloom graphs generated to their account for greater convenience",
    "Share the current graph project with another person",
  ],
  "Will Not Have": [
    "The ability to solely analyse a single taxonomy as opposed to comparing with one another",
    "The ability to switch between light and dark mode depending on the user preference",
  ],
};

export default moscowRefined;
