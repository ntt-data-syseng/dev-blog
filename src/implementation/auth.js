import React from "react";
import Iframe from "react-iframe";

import Figure from "../components/Figure";
import One from "../images/implementation/auth/one.png";
import Two from "../images/implementation/auth/two.png";
import Three from "../images/implementation/auth/three.png";
import Four from "../images/implementation/auth/four.png";
import Five from "../images/implementation/auth/five.png";
import Six from "../images/implementation/auth/six.png";
import Seven from "../images/implementation/auth/seven.png";
import Eight from "../images/implementation/auth/eight.png";
import Nine from "../images/implementation/auth/nine.png";
import Ten from "../images/implementation/auth/ten.png";
import Eleven from "../images/implementation/auth/eleven.png";

const auth = () => {
  return (
    <>
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>Service Overview</h1>
      <p>
        To enhance the user experience and provide extra features for recurring
        users the team decided to set up a simple authentication system with
        Node, Express and Postgresql. Redis was also implemented to maintain the
        session of the user and provide a faster lookup for the current logged
        user.
        <br />
        <br />
      </p>
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>The Code</h1>
      <p>
        The first stage was to create a postgresql database to store the users
        information, before creating the table required as seen below.
        <br />
      </p>
      <div className="flex flex-row space-x-12">
        <Figure img={One} figureContent="Figure 1" />
      </div>
      <br />
      <br />
      <p>
        The password needed to be stored, hence when a new user is created in
        the ‘createUser’ function the passwords were also encrypted with bcrypt
        and then stored in our postgress database, as seen below in figure 2:
        <br />
      </p>
      <div className="flex flex-row space-x-12">
        <Figure img={Two} figureContent="Figure 2" />
      </div>
      <p>
        The login function checks that the user is able to login with the
        credentials submitted. Figure 3 below shows how it words.
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Three} figureContent="Figure 3" />
      </div>
      <br />
      <br />
      <p>
        The line in figure 4 checks to see if the user email is found in the
        database.
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Four} figureContent="Figure 4" />
      </div>
      <br />
      <br />
      <p>
        If the email can’t be found the code sends a json message and hence the
        user cannot login, as seen in figure 5:
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Five} figureContent="Figure 5" />
      </div>

      <br />
      <br />

      <p>
        If the user email is found the password entered is compared with the
        password using the compare function of bcrypt as seen below in figure 6,
        generating an Incorrect password message of the password entered and the
        password stored are not the same:
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Six} figureContent="Figure 6" />
      </div>
      <br />
      <br />
      <p>
        If the user enters an email and password that both match to ones stored
        in the database then the session is created in redis and the session
        cookie is sent as seen below in figure 7 where the req.session object is
        set to the authenticated user's information:
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Seven} figureContent="Figure 7" />
      </div>
      <br />
      <br />
      <p>
        The logout function is simply deleting the session cookie on redis and
        we also delete the connect.sid cookie on the client side so that it
        can't be used for subsequent API calls, the flow diagram in figure 8
        shows how it works.
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Eight} figureContent="Figure 8" />
      </div>
      <br />
      <br />
      <p>This is implemented with the following code in figure 9:</p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Nine} figureContent="Figure 9" />
      </div>
      <br />
      <br />
      <p>
        If the user was not already logged in, they are unable to logout, hence
        a error message is sent as seen below in figure 10:
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Ten} figureContent="Figure 10" />
      </div>
      <br />
      <br />
      <p>
        The code also ensures the emails stored will always be unique within the
        ‘User’ class. This can be seen in figure 11:
      </p>
      <br />
      <div className="flex flex-row space-x-12">
        <Figure img={Eleven} figureContent="Figure 11" />
      </div>

      <br />
      <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
        You can interact with the iframe below of the ntt-auth through the usage
        of swagger documentation to test out of implementation. If the iframe
        does not load please proceed to http://20.213.242.240/api/auth/docs//
      </h1>

      <br />
      <div className="border-red">
        <Iframe
          url="http://20.213.242.240/api/auth/docs//"
          allowtransparency="true"
          style={{ background: "red" }}
          width="100%"
          height="450px"
          className="border-red"
          display="initial"
          position="relative"
        />
      </div>
    </>
  );
};

export default auth;
