import React from "react";
import tw, { styled } from "twin.macro";
import Brand from "./brand";
import NavLinks from "./navLinks";
import { Link } from "react-router-dom";
import useToggle from "../../hooks/useToggle";

const Container = styled.nav`
  ${tw`w-full z-30 top-0 w-full flex flex-wrap items-center justify-between mt-0 px-2 bg-white`}
`;

const Index = ({ links }) => {
  const [value, toggle] = useToggle();
  return (
    <Container>
      <Link to="/">
        <Brand brandName={"NTT Data"} />
      </Link>

      <div className="block lg:hidden pr-4">
        <button
          id="nav-toggle"
          className="flex items-center px-3 py-2 border rounded text-gray-500 border-gray-600 hover:text-gray-800 hover:border-green-500 appearance-none focus:outline-none"
          onClick={toggle}
        >
          <svg
            className="fill-current h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>

      {value && (
        <div className="inline-block w-full lg:hidden">
          <NavLinks names={links} />
        </div>
      )}

      <div className="hidden lg:block">
        <NavLinks names={links} />
      </div>
    </Container>
  );
};

export default Index;
