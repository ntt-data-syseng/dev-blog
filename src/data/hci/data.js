import First from "../../images/hci/iterOne/first.png";
import Second from "../../images/hci/iterOne/second.png";
import Three from "../../images/hci/iterOne/three.png";
import Fourth from "../../images/hci/iterOne/four.png";
import Fifth from "../../images/hci/iterOne/fifth.png";
import Sixth from "../../images/hci/iterOne/sixth.png";
import Alfred from "../../images/hci/personas/alfred.png";
import Olivia from "../../images/hci/personas/olivia.png";

import LandingPage from "../../images/hci/figma/landingPage.png";
import bloomGraph from "../../images/hci/figma/bloomGraph.png";
import bloomGraphFilter from "../../images/hci/figma/bloomGraphFilter.png";
import bloomGraphNotes from "../../images/hci/figma/bloomGraphNotes.png";
import LoginPage from "../../images/hci/figma/loginPage.png";
import SignupPage from "../../images/hci/figma/signUpPage.png";
import Profile from "../../images/hci/figma/profile.png";
import BloomGraphUnique from "../../images/hci/figma/bloomGraphUnique.png";

export const sketches = [First, Second, Three, Fourth, Fifth, Sixth];
export const personas = [Alfred, Olivia];

export const figmas = [
  LandingPage,
  bloomGraph,
  bloomGraphFilter,
  bloomGraphNotes,
  LoginPage,
  SignupPage,
  Profile,
  BloomGraphUnique,
];
