import React from "react";
import PageTitle from "../components/PageTitle";
import Section from "../components/Section";
import SystemArchitectureDiagram from "../images/systemdesign/system-design-architecture-detailed.png";
import SiteMap from "../images/systemdesign/site-map.png";
import ERD from "../images/systemdesign/erd.png";
import Dataflow from "../images/systemdesign/dataflow.png";
import Flow from "../images/systemdesign/flow.png";

const SystemDesign = () => {
  return (
    <>
      <PageTitle title="System Design" />

      <Section title="System Architecture Diagram" shade>
        <img
          src={SystemArchitectureDiagram}
          style={{ marginBottom: "50px", marginTop: "30px" }}
        />
        <p>
          The Gene Cluster Analysis (GCA) platform is a web application
          consisting of several different components. We have split our
          application into related responsibility repositories. The two
          overarching separations are the frontend and the backend. The frontend
          is built with NextJS, utilising the G6 library to help with rendering
          the graph data in an interactive manner. The backend is built with
          node and python, with the logic of handling each backend tasks are
          further split into smaller repositories.
          <br />
          <br />
          At the entrance we have NTT-Gateway which maps the appropriate
          requests to each backend service.
          <br />
          <br />
          <h1 style={{ fontSize: "17px", fontWeight: "800" }}>NTT-GRAPH</h1>
          NTT-GRAPH is the main repository that handles the logic of the
          application. It is built using Fastapi with Neo4j and PostgresQL as
          its database. The service performs the core logic of the application
          which includes searching for information on the taxonomies and
          generating the analysis between two taxonomies. It also has a
          bookmarking functionality that stores the analysis that each of the
          authenticated users has done for ease of coming back and finding it
          later, as well as ranking the most popular analysis.
          <br />
          <br />
          The information of the genome and taxonomies are stored inside the
          graph database. Whereas the bookmarking and ranking of analysis are
          stored in traditional relational databases with postgres.
          <br />
          <br />
          <h1 style={{ fontSize: "17px", fontWeight: "800" }}>NTT-AUTH</h1>
          NTT-AUTH is the authentication service which is built using expressjs,
          postgres and uses redis as a session engine store. Other than creating
          and authenticating the user, it has an authCheck route which performs
          the role of extending the user’s timeout session if they are currently
          authenticated. This authCheck route is called during each call to the
          NTT-Gateway and its response, which is the user’s information is put
          into the request object to be passed onto other services.
          <br />
          <br />
          <h1 style={{ fontSize: "17px", fontWeight: "800" }}>NTT-PDF</h1>
          NTT-PDF is the pdf generation service which is built using expressjs
          and uses jsPDF with the autotablePlugin to help in creating
          downloadable PDF. This service is intended for use with the NTT-GRAPH
          service which passes the analysis information to it. For each analysis
          it generates a unique identifier for the pdf and returns the file
          streamable object response. It also stores caches of previously
          created PDF so that the same analysis report does not need to be
          recreated.
        </p>
      </Section>

      <Section title={"Detail Architecture and Data Flow"}>
        The flow of our architecture can be seen in the figure below and brief
        explanation of what each services does.
        <br />
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>Nginx Service</h1>
        <p>
          Everything is served through the nginx service which acts as a reverse
          proxy, and caching possible services. Nginx proxy only two routes in
          our application the api and the frontend service. All API Routes are
          appended with the /api/servicename prefix. The default route / will
          route the user to the frontend service. Nginx also performs basic
          caching for the frontend which is just a static frontend application
          which could reduce the load on the server.
        </p>
        <br />
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>NTT-Gateway</h1>
        <p>
          NTT-Gateway perform similar tasks to nginx in that it proxies
          appropriate requests the an appropriate resource. However, we need a
          separate service for this because in our NTT-Gateway it also performs
          a simple authentication check on each user's requests. This will
          populate the HTTP Req object if the user is authenticated and increase
          their user's session (session are like countdown time if you do not
          return to a website for sometime you are locked out). By populating
          the Req object we decouple the authentication functionality from each
          respective service. For example NTT-Graph or any other service except
          NTT-Auth needs to perform the authentication, they could just simply
          check that the Request object is populated with an appropriate
          credentials or not, and this is assumed to be correct since it is not
          possible in our application fo the user to access each individual
          service since they are hidden behind a proxy and not available outside
          of our docker network. As an example of a request /api/auth will map
          to NTT-Auth and /api/graph will map to NTT-Graph.
        </p>
        <br />
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>NTT-Graph</h1>
        <p>
          NTT-Graph performs the main analysis of our application. It generates
          common features of the virus data that has been inserted and linked by
          reading the csv data. For our task, the client only needs viruses in
          the coronavaridae family but this application could be inserted with
          other viruses as well. Currently the setup inserts the data on each
          reboot of the application. It uses the Neo4j graph database which the
          virus data are stored on. One of the reasons we built this service on
          FastAPI(Python based library) was because there is a simple to use ORM
          library built to be used with Neo4j called Neomodel. In addition to
          this, this service is also connected to the Postgres database to
          perform side tasks such as the bookmarking the user's saved analysis
          as well as the frequency of the analysis. These two are extra
          convenient features that allow the researcher or students to return to
          the application and quickly bring up the analysis that they have made
          in the past, since remembering viruses combination names are often
          long. The frequency tracker also assists them to know what other
          analysis people are working on so that their could perhaps ask to join
          in to assist in the research or simply have knowledge of the trend.
        </p>
        <br />
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>NTT-Auth</h1>
        <p>
          NTT-Auth is our authentication service that allow users to
          authenticate themselves as well as create their accounts. This service
          is also depended on by the NTT-Gateway as it performs authentication
          on each requests to either kill the user session, and prevent further
          access to protected routes, if they are not authenticated, or extend
          the user's session by increasing the ttl(time to live) of the
          credentials in redis which this service utilises.
        </p>
        <br />
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>NTT-PDF</h1>
        <p>
          NTT-PDF handles the PDF generation and caching of our application. It
          works by receiving the content of the analysis generated by NTT-Graph.
          From this data information it either retrieves the cached(old) PDF
          that was previously generated or generated a brand new PDF and returns
          that whilst simultaneously storing it for next use. This solution
          allows for quick PDF generation for more common analysis performed
          without pre-computation(Pre generating all pdf combinations that are
          available). The caching for this service is simply completeed by
          writing the file to the file system and tagging it with the
          combination of the two viruses taxonomy id, since no two viruses can
          have the same id. When this service retrieves the analysis content it
          checks whether the combination of these two ids are available in the
          cache, if so it would return it.
        </p>
        <br />
        <br />
        <h1 style={{ fontSize: "20px", fontWeight: "800" }}>FRONTEND</h1>
        <p>
          The frontend of our application is served through the default path /
          on port 80(which is the default port so it is not shown in the URL).
          It is built with NextJS and it performs appropriate request to the
          resources that it needs. For simplicity the backend URL points to the
          /api/service name instead of having to point to rest endpoints on
          separate ports if these were not hosted with nginx to map the
          appropriate endpoints and port numbers.
        </p>
        <br />
        <br />
        <img src={Dataflow} />
      </Section>

      <Section title={"Relational Database Schema"} shade>
        For our authentication and bookmarking system, we utilised relational
        database through Postgres. Below is the entity relation diagram of the
        database. For this project SQLAlchemy as the ORM(Object Relational
        Mapping) to construct the tables as Python classes. The figure below
        illustrates the layout of the database for this feature of the
        application. Each user are first created when they create a new user.
        Each analysis done by the user will be incremented in the Analysis
        table. The user are then able to bookmark the analysis that were made.
        (It does not make sense to allow them to bookmark an analysis that has
        not yet occurred). A user can create many bookmarks, but a single unique
        bookmark can only belong to one user. Furthermore, the same analysis can
        be in multiple bookmarks and a single bookmark can only contain one
        analysis.
        <img src={ERD} />
      </Section>

      <Section title={"User Flow"}>
        To make it clearer to understand the interactions that the user can make
        from their perspective, the action flow diagram of the user is shown
        below
        <br />
        <br />
        <img src={Flow} />
      </Section>

      <Section title={"Site Map"} shade>
        <p>
          The site map below illustrates the structure of our web application.
          The home page is the centralised component of the application where
          the core functionalities are placed. This is where a user can
          essentially carry out their analysis through selecting different
          organisms and generating a corresponding bloom graph. From here the
          user is able to generate a pdf report of their analysis, share a link
          of this specific analysis graph or bookmark the analysis. Some of
          these features are restricted for authenticated users only. Users can
          login or sign up via a separate authentication page. These
          authenticated users will then gain access to a separate dashboard
          page, where they can view the most popular bookmarked analysis’ as
          well as their own bookmarked findings.
        </p>
        <img
          src={SiteMap}
          style={{ marginBottom: "50px", marginTop: "30px" }}
        />
      </Section>
      <Section title={"Design Patterns"}>
        <p>
          For our application we have two overarching design patterns. For the
          frontend, NextJS being based on React we followed the functional
          programming paradign. In terms of immutability and creating pure
          functions like click handler and action creators, as will be
          illustrated in the implementation section of the frontend.
          <br />
          <br />
          For the other services we followed a simple MVC
          (Model-View-Controller) paradigm <br />
          <br />
          <b>Model</b> <br />
          <br />
          The model of this design pattern handles all the data related logic of
          the application. In our context in NTT-AUTH and NTT-GRAPH (graph-db)
          it is the ORM which are being used. For NTT-Auth that would be
          Sequelize and for NTT-Graph(Neomodel and SQLAlchemy).
          <br />
          <br />
          <b>View</b> <br />
          <br />
          The view of this design paradigm are the resources exposed to the user
          in our context. It exposes the rest endpoints that the client can
          query for actions to be completed.
          <br />
          <br />
          <b>Controller</b> <br />
          <br />
          The controller is the interface between the view and the model. It
          processes incoming data from the view, prepares it, does any action it
          needs and minipulates the data using the Model. For example in
          NTT-GRAPH(GRAPH-DB) the request for an analysis will be requested at
          the endpoint, analysis ids are first verified that they are indeed
          valid and that they exists in the database before being manipulated by
          the Model from the Controller.
        </p>
      </Section>
      <Section title={"API Definition"} shad>
        <p>
          API Definition for each submodule services can be found in the
          implementation section of each respective service.
        </p>
      </Section>
    </>
  );
};

export default SystemDesign;
