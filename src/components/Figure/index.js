import React from "react";

const index = ({ img, figureContent }) => {
  return (
    <div className="mx-auto">
      <img src={img} style={{ marginBottom: "30px", marginTop: "30px", width: '100%', maxHeight: '1080px', maxWidth: '1920px' }} />

      <h1 className="text-center font-bold">{figureContent}</h1>
    </div>
  );
};

export default index;
