import React, { useState } from "react";
import PageTitle from "../components/PageTitle";

import Section from "../components/Section";
import MoscowAchievements from "../images/evaluation/moscowAchievmentList.png";
import Contribution from "../images/evaluation/contribution.png";
import BugList from "../images/evaluation/bugList.png";
import { criticalEvaluationTabs } from "../data/evaluation";

const Evaluation = () => {
  const [currentTab, setCurrentTab] = useState("User Experience");

  return (
    <>
      <PageTitle title="Evaluation" />

      <Section title="MoSCoW List Achievement">
        <img src={MoscowAchievements} />
      </Section>

      <Section title="Individual Contribution" shade>
        <img src={Contribution} />
      </Section>

      <Section title="Critical Evaluation">
        We have evaluated our product on 6 overarching criterias in each tab
        <br />
        <div
          style={{
            flex: "column",
            display: "flex",
            width: "100%",
            margin: "20px auto",
            textAlign: "center",
            marginBottom: "15px",
          }}
        >
          {Object.keys(criticalEvaluationTabs).map((key) => {
            return (
              <div style={{ marginRight: "20px", marginleft: "20px" }}>
                <h1
                  style={{ fontSize: "17px", fontWeight: "800" }}
                  className={currentTab === key ? "underline" : ""}
                  onClick={() => setCurrentTab(key)}
                >
                  {key}
                </h1>
              </div>
            );
          })}
        </div>
        {criticalEvaluationTabs[currentTab].map((text) => {
          return (
            <div>
              <p>{text}</p>
              <br />
            </div>
          );
        })}
      </Section>

      <Section title="Bug List" shade>
        <p>
          As it currently stands, we have eradicated all identified bugs that
          have appeared throughout the development of our application.
          Therefore, we have decided to highlight some key issues we faced
          during the development process and how we came to overcome them
          through different solutions. This is detailed in the bug list table
          below.
        </p>
        <br />
        <img src={BugList} />
      </Section>

      <Section title="Future Work">
        <p>
          The team had many ideas to implement in the future to further improve
          the application.
          <br />
          <br />
          1. One of the ‘Should have’ requirements on the MoSCoW list was the
          ability for users to take notes. Due to time constraints the
          implementation of this was not carried out, however in future versions
          of the application we believe this would be beneficial for users to
          aid analysis.
          <br />
          <br />
          2. The team could also increase the size of the database but pulling
          more data from the KEGG databse and hence increase the number
          organisms available to provide more options for analysis.
          <br />
          <br />
          3. The application could also provide the option for the analysis of 3
          or more organisms at the same time, to again further the analysis and
          understanding of researchers.
          <br />
          <br />
          4. The application could also have a discussion forum where users are
          able to share notes and discuss a certain analysis to further
          collaboration.
          <br />
          <br />
          5. A version of the application that is compatible with phone browsers
          would also be a possible update, to enable researchers to access the
          application on the go.
          <br />
          <br />
          6. The authentication system is also a needed update to be
          strengthened. Google, Facebook, Apple and other services could also be
          incorporated to give users other ways to signup rather than via the
          signup form.
          <br />
          <br />
          7. The current architecture of the application needs the author to
          re-deploy their application manually everytime each change is made to
          one service (a change in a NTT-Auth service for example). These only
          affect the authentication part but as the current setup with the
          bitbucket repositories we must pull the repo from the MVP repositories
          and then rebuild that part only. This could mitigate by incorporating
          deployment with the Continuous Integration system that we had running
          for the application. We could make it so that only service self
          deploys after all the tests has ran successfully.
          <br />
          <br />
          8. Related to number 7 but when during deployment of the newer version
          of the services or the application there is downtime that the user
          cannot access the service. Although this is only 2-3 minutes when
          building the whole service it could be fixed by queueing the requests
          the user made when the services were being built and handling those
          requests when the new version is up and running completely by using
          something like celery.
        </p>
      </Section>
    </>
  );
};

export default Evaluation;
