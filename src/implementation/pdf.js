import React from 'react';

import Figure from "../components/Figure"
import One from "../images/implementation/pdf/one.png";
import Two from "../images/implementation/pdf/two.png";
import Three from "../images/implementation/pdf/three.png";
import Four from "../images/implementation/pdf/four.png";
import Five from "../images/implementation/pdf/five.png";
import Six from "../images/implementation/pdf/six.png";
import Seven from "../images/implementation/pdf/seven.png";
import Eight from "../images/implementation/pdf/eight.png";
import Nine from "../images/implementation/pdf/nine.png";
import Ten from "../images/implementation/pdf/ten.png";

const pdf = () => {
  return (
    <> 
  <h1 style={{ fontSize: "17px", fontWeight: "800" }}>
    Service Overview
  </h1>
  <p>
  The Library jsPDF was chosen to implement the PDF generation. The team initially implemented this on the frontend, 
  however later decided to move it to the backend. The reason for this is too pre-compute each pdf for different taxonomy 
  combinations and store them somewhere.
  <br />
</p>
<br />
<br />
<h1 style={{ fontSize: "17px", fontWeight: "800" }}>
  The Code
</h1>
<p>
    The logic for preparing the data from the bloom graph for the tables in the PDF report is all found in the ‘graph-db’ repository.
    <br />
    <br />
    The ‘prepareCommanNodesPdfFormat’ prepares the section of the report which lists the common nodes between the 2 organisms, 
    mapping the id, name and extra information about each common gene cluster to the appropriate column as seen below in figure 1, 
    with ‘miscellaneous’ returning the extra information:

  <br />
</p>
<div className="flex flex-row space-x-12">
  <Figure
  img={One}
  figureContent="Figure 1"
/>
</div>
<br />
<br />
<p>
    The ‘prepareSingleNodePdfFormat’ is the function displaying the unique nodes for the 2 separate organism if any. 
    The code for this is seen in figure 2:
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Two}
  figureContent="Figure 2"
/>
</div>
<br />
<br />
<p>
    The function ‘pdfDataSelectot’ puts all the information together gathered from the above new function 
    and forms the title of the report aswell as seen in figure 3:
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Three}
  figureContent="Figure 3"
/>
</div>
<br />
<br />
<p>
    A new repo was also set up and the library imported. The ‘generateTable’ function utilises ‘autoTable’ to create 
    the template for each of the three tables required. All tables will have three columns: id, name and information, 
    then mapping the appropriate information to the correct column. This can be seen in figure 4:
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Four}
  figureContent="Figure 4"
/>
</div>
<br />
<br />
<p>
    The table is then centred while the orientation, format and other settings such as font size are defined as seen in figure 5: 
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Five}
  figureContent="Figure 5"
/>
</div>
<br />
<br />
<p>
    A function to ensure there is data to actually populate the PDF report was also implemented, 
    returning an error if no data was found as seen in figure 6: 
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Six}
  figureContent="Figure 6"
/>
</div>
<br />
<br />
<p>
    The jsPDF library does not work in the backend out of the box, 
    even though it is stated as supported. Hence the window object was removed returning 
    to the node version as seen in figure 7:
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Seven}
  figureContent="Figure 7"
/>
</div>
<br />
<br />
<p>
    Code was also implemented to store the generated reports in cache, to prevent the need for repeated generation 
    every time as stated above. This enables a more efficient report generation as when the user requests for a pdf, 
    the application will just extract the pdf from storage and hand it to the user. Hence the pdf to be returned to 
    the client in one request/response cycle without needing to hand off the pdf generation to a background task queue. 
    If the required report is not found in the cache then the report is generated from scratch. 
    This can be seen implemented in figure 8:
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Eight}
  figureContent="Figure 8"
/>
</div>
<br />
<br />
<p>
    ‘Yup’ was also imported to validate the data coming in to make sure its valid. This is the first process of generating 
    the actual PDF. If the validation of the data is unsuccessful, then an error is thrown. This can be seen implemented 
    upon the initialisation steps below in the middleware folder of NTT-PDF in figure 9:
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Nine}
  figureContent="Figure 9"
/>
</div>
<br />
<br />
<p>
    As seen below the data must be validated for the PDF to be generated.
</p>
<br />
<div className="flex flex-row space-x-12">
  <Figure
  img={Ten}
  figureContent="Figure 10"
/>
</div>
<br />
<br />
<p>
    In summary, the data for the report is prepared in the backend (graph-db repository), 
    this data is then checked, and the PDF report is all formed in the NTT-PDF repository.
</p>
<br />
<br />
</>
  );
};

export default pdf;