import React from "react";
import tw from "twin.macro";

const Container = tw.div`container mx-auto text-center px-3 lg:px-0`;

const Title = tw.h1`my-4 text-2xl md:text-3xl lg:text-5xl font-black leading-tight text-white mb-8`;

const Subtitle = tw.p`leading-normal text-base text-sm mb-2 text-white`;

const index = ({ title, subtitles = [] }) => {
  return (
    <Container>
      <Title>{title}</Title>

      {subtitles.map((subtitle) => (
        <Subtitle>{subtitle}</Subtitle>
      ))}
    </Container>
  );
};

export default index;
